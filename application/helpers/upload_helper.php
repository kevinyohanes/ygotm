<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function file_upload($table = NULL, $id = NULL, $field = NULL) {
    
    $config['upload_path'] = './uploads/';
    $config['file_name'] = $table.$id;
    $config['allowed_types'] = 'jpeg|jpg|png';
    $config['max_size']  = '150';
    $config['max_width']  = '1024';
    $config['max_height']  = '1024';
    $config['overwrite'] = TRUE;
    
    $this->upload->initialize($config);

    if (!$this->upload->do_upload($field)) {
        return FALSE;
    } else {
        $data = array('upload-data' => $this->upload->data());
        return $data;
    }
}

?>