<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function generate_hash($var) {
    // More cost = more secure = more processing power
    $cost = 10;

    // Create a random salt (one-time key)
    $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');

    // $2a$ is Blowfish algorithm, the rest are cost parameter
    $salt = sprintf("$2a$%02d$", $cost) . $salt;

    // Hash $var with the salt
    $hash = crypt($var, $salt);
    return $hash;
}

function base64_url_encode ($input) {
    return strtr(base64_encode($input), '+/=', '._-');
}

function base64_url_decode ($input) {
    return base64_decode(strtr($input, '._-', '+/='));
}

?>