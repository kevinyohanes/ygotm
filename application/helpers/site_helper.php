<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('hash_equals')) {
    function hash_equals($str1, $str2) {
        if(strlen($str1) != strlen($str2)) {
            return false;
        } else {
            $res = $str1 ^ $str2;
            $ret = 0;
            for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);
            return !$ret;
        }
    }
}

if (!function_exists('tournament_type')) {
    function tournament_type ($id) {
        switch ($id) {
            case 1:
                return 'Swiss';
                break;
            case 2:
                return 'Single Elimination';
                break;            
            default:
                return FALSE;
                break;
        }
    }
}

if (!function_exists('arr_search_by_name')) {
    function arr_search_by_name($name, $array) {
        foreach ($array as $key => $val) {
            if ($val['name'] === $name) {
                return $key;
            }
        }
        return null;
    }
}

if (!function_exists('generate_swiss_matches')) {
    function generate_swiss_matches($tournament) {
        if ($tournament == NULL) {
            return FALSE;
        }
        
        // Load instance and models
        $CI = get_instance();
        $CI->load->model('matches_model');
        $CI->load->model('participants_model');

        // If 1st round Swiss, shuffle the array and generate match pairings
        // Close the registration after a successful generation
        if ($tournament['type'] == 1 && $tournament['current_rounds'] == 0) {
            $count = $CI->participants_model->count_approved_participants($tournament['id'], 'non');
            $participants = NULL;
            // Add Dummy account if odd
            if ($count % 2 != 0) {
                $CI->participants_model->set_dummy_participants($tournament['id']);
            }
            $participants = $CI->participants_model->get_approved_participants($tournament['id'], 'non');
            shuffle($participants);
            while ($participants != NULL) {
                $last_match = $CI->matches_model->set_swiss_matches($participants[0]['id'], $participants[1]['id'], $tournament['id'], 1);
                array_splice($participants, 0, 2);
            }
            // Pair advantage player with Dummy
            $adv = $CI->participants_model->get_approved_participants($tournament['id'], 'adv');
            if ($adv != NULL) {
                $dummy = $CI->participants_model->get_dummy_id($tournament['id']);
                if (empty($dummy)) {
                    $CI->participants_model->set_dummy_participants($tournament['id']);
                    $dummy = $CI->participants_model->get_dummy_id($tournament['id']);
                }
                while ($adv != NULL) {
                    $last_match = $CI->matches_model->set_swiss_matches($adv[0]['id'], $dummy, $tournament['id'], 1);
                    array_splice($adv, 0, 1);
                }
            }
            // Unapprove temporary Dummy if the original participants are even
            $count = $CI->participants_model->count_approved_participants($tournament['id']);
            if ($count % 2 != 0 && !empty($dummy)) {
                $CI->participants_model->unapprove_dummy($tournament['id']);
            }
            $CI->tournaments_model->inc_current_rounds($tournament['id']);
            $CI->tournaments_model->close_registration($tournament['id']);
            return TRUE;
        }
        // If 2nd and more round Swiss, sort the player by highest points, and then pair them
        // No player may meet the same opponent more than once
        else if ($tournament['type'] == 1 && $tournament['current_rounds'] > 0) {
            // Update the standings before anything
            $participants = $CI->participants_model->get_participants_ids($tournament['id']);
            foreach ($participants as $p) {
                $CI->participants_model->set_points($p);
                $CI->participants_model->set_mwp($p);
                $CI->participants_model->set_omw($p);
                $CI->participants_model->set_pgw($p);
                $CI->participants_model->set_ogw($p);
            }
            // Check if 'Dummy' is needed
            if (count($participants) % 2 != 0) {
                $CI->participants_model->set_dummy_participants($tournament['id']);
                $participants = $CI->participants_model->get_approved_participants($tournament['id']);
            }
            // Checks the ideal round of Swiss Tournament
            // log(2) [number of participants]
            $ideal_round = ceil(log(count($participants), 2));
            if ($tournament['current_rounds'] >= $ideal_round) {
                $CI->session->set_flashdata('msg', 'Ideal number of rounds is reached; which is the base 2 log of the number of players');
                redirect('/matches/manage/'.$tournament['id'], 'refresh');
            }
            
            // Pair the rest of the player
            $i = 1;            
            $participants = $CI->participants_model->get_participants_grouped($tournament['id']);
            $reserve = array();
            foreach ($participants as $g) {
                // Pair reserved player first
                while ($reserve != NULL) {
                    $player = $reserve[0];
                    $opponent = $g[$i - 1];
                    $prev_opponent = $CI->matches_model->previous_opponents($player);
                    if (in_array($opponent, $prev_opponent)) {
                        $i++;
                    } else {
                        $CI->matches_model->set_swiss_matches($player, $opponent, $tournament['id'], $tournament['current_rounds'] + 1);
                        $reserve = array_diff($reserve, [$player]);
                        $g = array_diff($g, [$opponent]);
                        $i = 1;
                        $reserve = array_values($reserve);
                        $g = array_values($g);
                    }
                }
                // Grouped Pairing
                while ($g != NULL) {
                    // Put the last player in reserve
                    if (count($g) == 1) {
                        $reserve[] = $g[0];
                        break;
                    }
                    $player = $g[0];
                    $opponent = $g[count($g) - $i];
                    $prev_opponent = $CI->matches_model->previous_opponents($player);
                    if (in_array($opponent, $prev_opponent)) {
                        $i++;
                    } else if ($player == $opponent) {
                        $reserve += $g;
                        $g = array();
                        $i = 1;
                    } else {
                        $CI->matches_model->set_swiss_matches($player, $opponent, $tournament['id'], $tournament['current_rounds'] + 1);
                        $g = array_diff($g, [$player, $opponent]);
                        $i = 1;
                        $g = array_values($g);
                        reset($g);
                    }
                }
            }

            $CI->tournaments_model->inc_current_rounds($tournament['id']);
            return TRUE;
        }
        return FALSE;
    }
}

if (!function_exists('generate_elim_matches')) {
    function generate_elim_matches($tournament) {
        if ($tournament == NULL) {
            return FALSE;
        }
        
        // Load instance and models
        $CI = get_instance();
        $CI->load->model('matches_model');
        $CI->load->model('participants_model');

        // 1st Round Elimination
        if ($tournament['current_rounds'] == 0) {
            $participants = $CI->participants_model->get_approved_participants($tournament['id']);
            shuffle($participants);
            $count = count($participants);
            $i = 1; $tmp = 0; $max = 0;
            while ($tmp <= $count) {
                $tmp = pow(2, $i++);
                if ($tmp > $count) {
                    // Do Nothing
                } else {
                    $max = $tmp;
                }
            }
            $playoff_num = $count - $max;
            $playoff_partics = array();
            while ($playoff_num > 0) {
                $playoff_partics[] = $participants[0];
                array_splice($participants, 0, 1);
                $playoff_num--;
            }
            // Begin pairing playoffs first
            foreach ($playoff_partics as $p) {
                $CI->matches_model->set_elim_matches($p['id'], $participants[0]['id'], $tournament['id'], 1);
                array_splice($participants, 0, 1);
            }
            // Pair the rest with Dummy
            $CI->participants_model->set_dummy_participants($tournament['id']);
            $dummy = $CI->participants_model->get_dummy_id($tournament['id']);
            if ($count == count($participants)){
                while ($participants != NULL) {
                    $CI->matches_model->set_elim_matches($participants[0]['id'], $participants[1]['id'], $tournament['id'], $tournament['current_rounds'] + 1);
                    array_splice($participants, 0, 2);
                }
            } else {
                foreach ($participants as $p) {
                    $CI->matches_model->set_elim_matches($p['id'], $dummy, $tournament['id'], 1);
                }
            }
            $CI->participants_model->unapprove_dummy($tournament['id']);
            $CI->tournaments_model->inc_current_rounds($tournament['id']);
            $CI->tournaments_model->close_registration($tournament['id']);
            return TRUE;
        }
        // 2nd and more Round Elimination
        else {
            // Update the standings before anything
            $participants = $CI->participants_model->get_participants_elim($tournament['id']);
            foreach ($participants as $p) {
                $CI->participants_model->set_points_elim($p['id']);
            }
            $participants = $CI->participants_model->get_participants_elim($tournament['id']);
            shuffle($participants);
            while ($participants != NULL) {
                $CI->matches_model->set_elim_matches($participants[0]['id'], $participants[1]['id'], $tournament['id'], $tournament['current_rounds'] + 1);
                array_splice($participants, 0, 2);
            }
            $CI->tournaments_model->inc_current_rounds($tournament['id']);
            return TRUE;
        }
    }
}

?>