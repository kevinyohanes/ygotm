<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>YIC Today : Print Matches</title>
    <style>
    @page {
        size: 21cm 29.7cm;
    }

    div.chapter, div.appendix {
        page-break-after: always;
    }
    </style>
    <!-- jQuery -->
    <link href="/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Bootstrap Core Files -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- Bootstrap Override -->
    <link href="/bootstrap/css/override.css" rel="stylesheet">
    <script src="/js/site.js"></script>
</head>
<body>
    <div class="main-container">
        <?php $i = 1; ?>
        <?php foreach ($matches as $match) : ?>
        <div class="matches-printout">
            <h3 class="text-center"><?php echo $tournament['name']; ?></h3>
            <h4 class="text-center"><?php echo 'Round '.$match['round'].' - Table '.$i; ?></h4>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%">Win</th>
                        <th>Name</th>
                        <th>Score</th>
                        <th>Signature</th>
                    </tr>    
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td><?php echo $match['partic1_name'] ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><?php echo $match['partic2_name'] ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>    
            </table>
        </div>
        <hr style="border-top: 1px solid #999;">
        <?php
        if ($i % 6 == 0) {
            echo('<div class="appendix"></div>');
        }
        $i++;
        endforeach;
        ?>
    </div>
    <script type="text/javascript">
    $(document).ready(function () {
        window.print();
    });
    </script>
</body>
</html>