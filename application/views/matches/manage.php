<a class="btn btn-primary" href="/tournaments/view/<?php echo $tournament['id'] ?>"> Go Back</a>
<a class="btn btn-success" href="/matches/generate/<?php echo $tournament['id'] ?>"> Generate Matches</a>
<a class="btn btn-success" href="/participants/update_standings/<?php echo $tournament['id'] ?>"> Update Standings</a>
<a class="btn btn-danger" href="/tournaments/reset/<?php echo $tournament['id'] ?>"> RESET</a>
<hr />
<ul class="nav nav-tabs">
<?php $i = 0; foreach ($matches as $round) : 
    // Take first match and get its round number
    $x = current($round);
    $round_num = $x['round'];
    ?>
    <li class="<?php if ($i == 0){echo 'active';} ?>" role="presentation">
        <a href="#round_<?php echo $round_num ?>" aria-controls="info" role="tab" data-toggle="tab">Round <?php echo $round_num ?></a>
    </li>
    <?php $i++; endforeach; ?>
</ul>

<div class="tab-content">
    <?php $i = 0; foreach ($matches as $round) : 
    // Take first match and get its round number
    $x = current($round);
    $round_num = $x['round'];    
    ?>
    <div role="tabpanel" class="tab-pane <?php if ($i == 0){echo 'active';} ?>" id="round_<?php echo $round_num ?>">
        <a class="btn btn-primary" style="margin-top: 10px;" href="/matches/printout/<?php echo $tournament['id'].'/'.$round_num ?>" target="_blank"> 
            <i class="glyphicon glyphicon-print"></i> Print
        </a>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Participant 1</th>
                    <th>P1 Score</th>
                    <th>P2 Score</th>
                    <th>Participant 2</th>
                    <th>Save Match</th>
                </tr>    
            </thead>
            <tbody>
                <?php foreach ($round as $match) : ?>
                <tr>
                    <td><?php echo $match['partic1_name'] ?></td>
                    <td><input type="number" max="2" id="partic1_score" style="width: 80px;" class="form-control score-input" value="<?php echo $match['partic1_score']; ?>"></td>
                    <td><input type="number" max="2" id="partic2_score" style="width: 80px;" class="form-control score-input" value="<?php echo $match['partic2_score']; ?>"></td>
                    <td><?php echo $match['partic2_name'] ?></td>
                    <td><button id="<?php echo $match['id'] ?>" type="submit" class="btn btn-success score-submit"><span class="glyphicon glyphicon-ok"></span> Submit</button></td>
                </tr>
                <?php endforeach; ?>
            </tbody>    
        </table>
    </div>
    <?php $i++; endforeach; ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // Check and set the score to 2 if exceed, or 0 if below
        $(".score-input").keyup(function() {
            var score = $(this).val();

            if (score > 2) {
                $(this).val(2);
            } else if (score < 0) {
                $(this).val(0);
            }
        });

        // Check the score validity before posting with AJAX
        $(".score-submit").click(function () {
            var field1 = $(this).parent().parent().find("#partic1_score");
            var field2 = $(this).parent().parent().find("#partic2_score");
            var s1 = parseInt(field1.val(), 10);
            var s2 = parseInt(field2.val(), 10);
            var t = s1 + s2;

            if (t > 3 || t < 2) {
                alert("Invalid Scores. The score must be 2-0, 2-1, 0-2, 1-2, or 1-1");
            } else {
                var target = '<?php echo base_url("index.php/matches/submit/") ?>';
                var id = $(this).attr("id");
                var self = this;
                $.ajax({
                    url: target,
                    type: "POST",
                    data: {'id': id, 'score1': s1, 'score2': s2},
                    success: function(response) {
                        console.log(response);
                        if (response == 1) {
                            field1.prop('disabled', true);
                            field2.prop('disabled', true);
                            $(self).attr('disabled', 'disabled');
                        } else {
                            alert("No update detected.");
                        }
                    },
                    error: function() {
                        alert("Something went wrong. Please refresh the page before trying again.");
                    }
                });
            }
        });
    });
</script>