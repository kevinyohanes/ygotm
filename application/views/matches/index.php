<?php
//print_r($matches);
?>

<ul class="nav nav-tabs">
<?php $i = 0; foreach ($matches as $round) : 
    // Take first match and get its round number
    $x = current($round);
    $round_num = $x['round'];
    ?>
    <li class="<?php if ($i == 0){echo 'active';} ?>" role="presentation">
        <a href="#round_<?php echo $round_num ?>" aria-controls="info" role="tab" data-toggle="tab">Round <?php echo $round_num ?></a>
    </li>
    <?php $i++; endforeach; ?>
</ul>

<div class="tab-content">
    <?php $i = 0; foreach ($matches as $round) : 
    // Take first match and get its round number
    $x = current($round);
    $round_num = $x['round'];
    ?>
    <div role="tabpanel" class="tab-pane <?php if ($i == 0){echo 'active';} ?>" id="round_<?php echo $round_num ?>">
        <div class="matches-list">
            <h2>Round <?php echo $round_num ?></h2>
            <?php foreach ($round as $match) : ?>
            <div class="matches-list-item row">
                <div class="col-xs-4">
                    <img src="/img/yugi.png" class="img-responsive center-block" />
                    <h3 class="text-center"><?php echo $match['partic1_name'] ?></h3>
                </div>
                <div class="col-xs-4">
                    <h3 class="text-center scores">
                        <?php echo $match['partic1_score'] ?> - <?php echo $match['partic2_score'] ?>
                    </h3>
                </div>
                <div class="col-xs-4">
                    <img src="/img/yugi.png" class="img-responsive center-block" />
                    <h3 class="text-center"><?php echo $match['partic2_name'] ?></h3>
                </div>
            </div>    
            <?php endforeach; ?>
        </div>        
    </div>
    <?php $i++; endforeach; ?>
</div>