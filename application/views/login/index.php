<?php
echo validation_errors(
    '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>',
    '</div>'
    );

if(isset($login_error)) {
    echo '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    '.$login_error.'
    </div>';
}
?>


<div class="login-box">
    <?php echo form_open('login/', 'class="form-horizontal" enctype="multipart/form-data"'); ?>
        <div class="form-group">
            <label class="sr-only" for="email_login">Email address</label>
            <input type="email" name="email_login" class="form-control" placeholder="Email address" required />
        </div>
        <div class="form-group">
            <label class="sr-only" for="password_login">Password</label>
            <input type="password" name="password_login" class="form-control" placeholder="Password" required />
            <div class="help-block text-right"><a href="#">Forget the password ?</a></div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
            <a href="/index.php/users/create" class="btn btn-primary btn-block">Register</a>
        </div>
        <div class="checkbox">
            <label>
            <input type="checkbox"> keep me logged-in
            </label>
        </div>
    </form>
</div>