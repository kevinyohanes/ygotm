<?php
//var_dump($users_item);
?>

<div class="profile-container row">
    <div class="profile-sidebar col-sm-3">
        <?php if ($users_item['photo'] == NULL) { ?>
        <div class="profile-picture" style="background-image: url(/img/yugi.png)"></div>
        <?php } else { ?>
        <div class="profile-picture" style="background-image: url(<?php echo $users_item['photo'] ?>)"></div>
        <?php } ?>
        <p>
            <?php echo $users_item['name'] ?> <br />            
            <?php echo $users_item['city'] ?>
        </p>
        <div class="profile-menu">
            <a href="mailto:<?php echo $users_item['email'] ?>"><span class="glyphicon glyphicon-envelope"></span> Send Email</a>
            <?php if (isset($_SESSION['logged_user'])) { ?>
            <?php if ($_SESSION['logged_user']['id'] == $users_item['id']) { ?>
                <?php if ($_SESSION['logged_user']['type'] == 1) { ?>
                <a href="/users/resend_verification/<?php echo $users_item['id'] ?>"><span class="glyphicon glyphicon-bookmark"></span> Resend Verification Link</a>
                <?php } ?>
                <?php if ($_SESSION['logged_user']['type'] != 1) { ?>
                <a href="/tournaments/manage/<?php echo $users_item['id']; ?>"><span class="glyphicon glyphicon-book"></span> Held Tournaments</a>
                <?php } ?>
                <?php if ($_SESSION['logged_user']['type'] == 0 || $_SESSION['logged_user']['type'] == 3) { ?>
                <a href="/articles/manage/<?php echo $users_item['id']; ?>"><span class="glyphicon glyphicon-book"></span> Written Articles</a>
                <?php } ?>
                <?php if ($_SESSION['logged_user']['type'] == 0) { ?>
                <a href="/banners/manage/"><span class="glyphicon glyphicon-bookmark"></span> Banners</a>
                <?php } ?>
            <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="profile-main col-sm-9">
        <div class="stats row">
            <div class="stats-item col-xs-4">
                <p class="stats-number">N/A</p>
                <p>Matches Won</p>
            </div>
            <div class="stats-item col-xs-4">
                <p class="stats-number">N/A</p>
                <p>Games Won</p>
            </div>
            <div class="stats-item col-xs-4">
                <p class="stats-number">N/A</p>
                <p>Championships Won</p>
            </div>
        </div>
        <h3>Duelist Information</h3>
        <div class="general-info row">
            <div class="col-xs-3">Full Name</div>
            <div class="col-xs-9">: <?php echo $users_item['name'] ?></div>
            <div class="col-xs-3">Email Address</div>
            <div class="col-xs-9">: <?php echo $users_item['email'] ?></div>
            <div class="col-xs-3">Birthdate</div>
            <div class="col-xs-9">: <?php echo date('j F Y', strtotime($users_item['birthdate'])) ?> <br /></div>
            <div class="col-xs-3">Address</div>
            <div class="col-xs-9">: <?php echo $users_item['address'] ?></div>
            <div class="col-xs-3">City</div>
            <div class="col-xs-9">: <?php echo $users_item['city'] ?></div>
            <div class="col-xs-3">Phone Number</div>
            <div class="col-xs-9">: <?php echo $users_item['phone'] ?></div>
        </div>
    </div>
</div>