<?php
echo validation_errors(
    '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
    '</div>'
    );
?>

<?php echo form_open('users/create', 'class="form-horizontal" enctype="multipart/form-data"'); ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" placeholder="John Doe" required />
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">Birthdate</label>
        <div class="col-sm-10">
            <input type="date" name="birthdate" class="form-control" required />
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" class="form-control" placeholder="johndoe@email.com" required />
        </div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <input type="password" name="password" class="form-control original-password" required />
            <p class="help-block password-alert" style="display: none;"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="confirm_password" class="col-sm-2 control-label">Confirm Password</label>
        <div class="col-sm-10">
            <input type="password" name="confirm_password" class="form-control confirm-password" required />
            <p class="help-block confirm-alert" style="display: none;"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="personal_number" class="col-sm-2 control-label">Personal Number</label>
        <div class="col-sm-10">
            <input type="text" name="personal_number" class="form-control" placeholder="KTP/SIM (Optional)" />
        </div>
    </div>
    <div class="form-group">
        <label for="phone" class="col-sm-2 control-label">Phone</label>
        <div class="col-sm-10">
            <input type="text" name="phone" class="form-control" placeholder="08**********" required />
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Address</label>
        <div class="col-sm-10">
            <textarea name="address" rows="3" class="form-control" ></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="city" class="col-sm-2 control-label">City</label>
        <div class="col-sm-10">
            <select class="form-control" name="city">
                <?php foreach ($cities as $city) : ?>
                <option value="<?php echo($city['id']); ?>"><?php echo($city['name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-10">
            <input type="file" name="photo" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="g-recaptcha" data-sitekey="6Lc-IzMUAAAAAP-zI7V6aQvFW6zSbuAbrk00J1EF"></div>
            <button type="submit" class="btn btn-primary btn-register" disabled="disabled">Register</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    // Validate passwords to match the regular expressions
    function validatePassword() {
        var password = $(".original-password").val();
        // Regex: at least 8 alphanumeric & special symbols, must contains at least numbers AND letters
        var regex = /^(?=.*[0-9])(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%^&*]{8,}$/;
        $(".password-alert").show();

        if (password.length < 8) {
            $(".password-alert").html("Passwords cannot be less than 8 characters");
            $(".btn-register").attr("disabled", "disabled");
        } else if (!regex.test(password)) {
            $(".password-alert").html("Passwords must contain at least letters and numbers.");
            $(".btn-register").attr("disabled", "disabled");
        } else {
            $(".password-alert").hide();
            $(".btn-register").removeAttr("disabled");
        }
    }

    // Compare the original password to confirmation password
    function checkPasswordMatch() {
        var password = $(".original-password").val();
        var confirmPassword = $(".confirm-password").val();
        $(".confirm-alert").show();

        if (password != confirmPassword) {
            $(".confirm-alert").html("Passwords do not match.");
            $(".btn-register").attr("disabled", "disabled");
        } else {
            $(".confirm-alert").html("Passwords match.");
            $(".btn-register").removeAttr("disabled");
        }
    }

    // Triggers
    $(document).ready(function() {
        // trigger validatePassword() while typing
        $(".original-password").keyup(validatePassword);
        // trigger checkPasswordMatch() while typing
        $(".confirm-password").keyup(checkPasswordMatch);
    });
</script>