<ul class="people-list">
    <?php foreach ($users as $users_item) : ?>
    <li class="people-list-item">
        <a href="<?php echo '/users/'.$users_item['id'] ?>">
            <div class="people-thumbnail">
                <img src="/img/yugi.png" class="img-responsive center-block" />
            </div>
            <p class="people-info">
                <?php echo $users_item['username']; ?>
                <br />
                <?php echo $users_item['city']; ?>
            </p>
        </a>
    </li>
    <?php endforeach; ?>
</ul>