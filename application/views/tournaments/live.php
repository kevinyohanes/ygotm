<!--
// Muaz Khan         - www.MuazKhan.com
// MIT License       - www.WebRTC-Experiment.com/licence
// Experiments       - github.com/muaz-khan/WebRTC-Experiment
-->
<article>
    <div class="github-stargazers"></div>

    <table class="visible">
        <tr>
            <td style="text-align: right;">
                <input type="hidden" id="conference-name" value="<?php echo $tournaments_item['name'] ?>">
            </td>
            <td>
                <button id="start-conferencing">New Broadcast</button>
            </td>
        </tr>
    </table>
    <table id="rooms-list" class="visible"></table>

    <div id="participants"></div>

    <script src="/js/socket.io.js"></script>
    <script src="/js/RTCPeerConnection-v1.5.js"></script>
    <script src="/js/broadcast.js"></script>
    <script src="/js/broadcast-ui.js"></script>
</article>