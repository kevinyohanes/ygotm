<?php
echo validation_errors(
    '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
    '</div>'
    );
?>

<?php echo form_open('tournaments/edit/'.$tournaments_item['id'], 'class="form-horizontal" enctype="multipart/form-data"'); ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" placeholder="Yu-Gi-Oh! Tournament" required value="<?php echo $tournaments_item['name'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">Tournament System</label>
        <div class="col-sm-10">
            <select class="form-control" name="type">
                <option value="1" <?php if ($tournaments_item['type'] == 1) {echo 'selected';} ?>>Swiss</option>
                <option value="2" <?php if ($tournaments_item['type'] == 2) {echo 'selected';} ?>>Single Elimination</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">Start Time</label>
        <div class="col-sm-10">
            <input type="datetime-local" name="start_time" class="form-control" required value="<?php echo date('Y-m-d\TH:i:s', strtotime($tournaments_item['start_time'])); ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">End Time</label>
        <div class="col-sm-10">
            <input type="datetime-local" name="end_time" class="form-control" required value="<?php echo date('Y-m-d\TH:i:s', strtotime($tournaments_item['end_time'])); ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Max Participants</label>
        <div class="col-sm-10">
            <input type="number" name="max_partic" class="form-control" required value="<?php echo $tournaments_item['max_partic'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Max Rounds*</label>
        <div class="col-sm-10">
            <input type="number" name="max_rounds" class="form-control" value="<?php echo $tournaments_item['max_rounds'] ?>" />
            <p class="help-block">* Only for Swiss System Tournaments. Left blank for infinite number of rounds.</p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Address</label>
        <div class="col-sm-10">
            <textarea name="address" rows="3" class="form-control"><?php echo $tournaments_item['address'] ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="youtube_channel_id" class="col-sm-2 control-label">Youtube Channel ID (Livestream)</label>
        <div class="col-sm-10">
            <input type="text" name="youtube_channel_id" class="form-control" placeholder="visit: youtube.com/account_advanced" value="<?php echo $tournaments_item['youtube_channel_id'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="city" class="col-sm-2 control-label">City</label>
        <div class="col-sm-10">
            <select class="form-control" name="city">
                <?php foreach ($cities as $city) : ?>
                <option value="<?php echo($city['id']); ?>" <?php if ($city['name'] == $tournaments_item['city']) {echo 'selected';} ?>><?php echo($city['name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
            <textarea name="description" rows="3" class="form-control"><?php echo $tournaments_item['description'] ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Mail Message*</label>
        <div class="col-sm-10">
            <textarea name="message" rows="3" class="form-control"><?php echo $tournaments_item['message'] ?></textarea>
            <p class="help-block">* Mail Message will be included in the email confirmation for your future participants in this tournament. You may use this for private matters, such as payment methods or bank account details.</p>
        </div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">Logo</label>
        <div class="col-sm-10">
            <img src="<?php echo $tournaments_item['logo'] ?>" width="200" />
            <input type="file" name="logo" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary btn-register">Save</button>
            <a href="/tournaments/<?php echo $tournaments_item['id'] ?>" type="submit" class="btn btn-register">Cancel</a>
        </div>
    </div>
</form>