<?php
echo validation_errors(
    '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
    '</div>'
    );
?>

<?php echo form_open('tournaments/create', 'class="form-horizontal" enctype="multipart/form-data"'); ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" placeholder="Yu-Gi-Oh! Tournament" required />
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">Tournament System</label>
        <div class="col-sm-10">
            <select class="form-control" name="type">
                <option value="1">Swiss</option>
                <option value="2">Single Elimination</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">Start Time</label>
        <div class="col-sm-10">
            <input type="datetime-local" name="start_time" class="form-control" required />
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">End Time</label>
        <div class="col-sm-10">
            <input type="datetime-local" name="end_time" class="form-control" required />
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Max Participants</label>
        <div class="col-sm-10">
            <input type="number" name="max_partic" class="form-control" required />
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Max Rounds*</label>
        <div class="col-sm-10">
            <input type="number" name="max_rounds" class="form-control" />
            <p class="help-block">* Only for Swiss System Tournaments. Left blank for infinite number of rounds.</p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Address</label>
        <div class="col-sm-10">
            <textarea name="address" rows="3" class="form-control"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="youtube_channel_id" class="col-sm-2 control-label">Youtube Channel ID (Livestream)</label>
        <div class="col-sm-10">
            <input type="text" name="youtube_channel_id" class="form-control" placeholder="visit: youtube.com/account_advanced" />
        </div>
    </div>
    <div class="form-group">
        <label for="city" class="col-sm-2 control-label">City</label>
        <div class="col-sm-10">
            <select class="form-control" name="city">
                <?php foreach ($cities as $city) : ?>
                <option value="<?php echo($city['id']); ?>"><?php echo($city['name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
            <textarea name="description" rows="3" class="form-control"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Mail Message*</label>
        <div class="col-sm-10">
            <textarea name="message" rows="3" class="form-control"></textarea>
            <p class="help-block">* Mail Message will be included in the email confirmation for your future participants in this tournament. You may use this for private matters, such as payment methods or bank account details.</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary btn-register">Register</button>
        </div>
    </div>
</form>