<a href="/tournaments/create/" class="btn btn-primary">
    <i class="glyphicon glyphicon-pencil"></i> Create New Tournaments
</a>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Address</th>
            <th>Start Time</th>
        </tr>    
    </thead>
    <tbody>
        <?php foreach ($tournaments as $tournaments_item) : ?>
        <tr>
            <td><?php echo $tournaments_item['name'] ?></td>
            <td><?php echo tournament_type($tournaments_item['type']) ?></td>
            <td><?php echo $tournaments_item['address'] ?></td>
            <td><?php echo date_format(date_create($tournaments_item['start_time']), 'j F Y - H:i') ?></td>
            <td><a href="/tournaments/<?php echo $tournaments_item['id'] ?>" class="btn btn-success">View</a></td>
            <td><a href="/tournaments/edit/<?php echo $tournaments_item['id'] ?>" class="btn btn-primary">Edit</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>    
</table>