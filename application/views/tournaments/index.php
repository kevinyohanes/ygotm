<ul class="tournaments-list">
    <?php foreach ($tournaments as $tournaments_item) : ?>
    <li class="tournaments-list-item">
        <a href="<?php echo '/tournaments/'.$tournaments_item['id']; ?>">
            <h3><?php echo $tournaments_item['name']; ?></h3>
            <div class="tournament-details col-xs-3">
                <span class="glyphicon glyphicon-user"></span><br />
                Max Participants<br />
                <span class="highlight">
                <?php
                if ($tournaments_item['max_partic'] == 0) {
                    echo 'Unlimited';
                } else {
                    echo $tournaments_item['max_partic'];
                }
                ?>
                </span>
            </div>
            <div class="tournament-details col-xs-3">
                <span class="glyphicon glyphicon-tower"></span><br />
                Tournament System<br />
                <span class="highlight">
                <?php echo tournament_type($tournaments_item['type']); ?>
                </span>
            </div>
            <div class="tournament-details col-xs-3">
                <span class="glyphicon glyphicon-calendar"></span><br />
                Date &amp; Time<br />
                <span class="highlight">
                <?php echo date_format(date_create($tournaments_item['start_time']), 'j F Y H:i:s') ?>
                </span><br />
            </div>
            <div class="tournament-details col-xs-3">
                <span class="glyphicon glyphicon-briefcase"></span><br />
                Tournament Manager<br />
                <span class="highlight"><?php echo $tournaments_item['manager'] ?></span>
            </div>
            <div class="tournament-details col-xs-12">
                <span class="glyphicon glyphicon-home"></span><br />
                Location<br />
                <span class="highlight"><?php echo $tournaments_item['address'] ?></span>
            </div>
            <div class="clearfix"></div>
        </a>
    </li>
    <?php endforeach; ?>
</ul>