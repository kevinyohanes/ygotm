<div class="profile-container row">
    <div class="profile-sidebar col-sm-3">
        <?php if ($tournaments_item['manager_photo'] == NULL) { ?>
        <div class="profile-picture" style="background-image: url(/img/yugi.png)"></div>
        <?php } else { ?>
        <div class="profile-picture" style="background-image: url(<?php echo $tournaments_item['manager_photo'] ?>)"></div>
        <?php } ?>
        <p>
            <?php echo $tournaments_item['manager'] ?> <br />
            Tournament Manager
        </p>
        <div class="profile-menu">
            <center>
            <a href="<?php echo '/users/'.$tournaments_item['manager_id'] ?>"><span class="glyphicon glyphicon-user"></span> View Profile</a>
            </center>
        </div>
    </div>

    <div class="profile-main col-sm-9">
        <?php if($tournaments_item['logo'] == NULL) { ?>
        <center><img src="/img/logo.png" height="150" /></center>
        <?php } else { ?>
        <center><img src="<?php echo $tournaments_item['logo'] ?>" height="150" /></center>
        <?php } ?>
        <center><h3><?php echo $tournaments_item['name'] ?></h3></center>
        <div class="general-info row">
            <div class="col-xs-3">Tournament System</div>
            <div class="col-xs-9">: <?php echo tournament_type($tournaments_item['type']); ?></div>
            <div class="col-xs-3">Date/Time</div>
            <div class="col-xs-9">
                : <?php echo date_format(date_create($tournaments_item['start_time']), 'j F Y H:i:s') ?>
                - <?php echo date_format(date_create($tournaments_item['end_time']), 'j F Y H:i:s') ?>
            </div>
            <div class="col-xs-3">Max Participants</div>
            <div class="col-xs-9">: <?php echo $tournaments_item['max_partic'] ?> <br /></div>
            <div class="col-xs-3">Max Rounds</div>
            <div class="col-xs-9">:
                <?php
                if (empty($tournaments_item['max_rounds']) || $tournaments_item['max_rounds'] == 0) {
                    echo 'Undefined';
                } else {
                    echo $tournaments_item['max_rounds'];
                }
                ?>
                <br />
            </div>
            <div class="col-xs-3">Address</div>
            <div class="col-xs-9">: <?php echo $tournaments_item['address'] ?></div>
            <div class="col-xs-3">City</div>
            <div class="col-xs-9">: <?php echo $tournaments_item['city'] ?></div>
        </div>
    </div>
    <div class="button-group">
        <?php if (count($participants) >= $tournaments_item['max_partic'] || !$tournaments_item['open']) { ?>
        <a class="btn btn-primary" role="button" disabled="disabled">Registration Closed</a>
        <?php } else { ?>
        <a class="btn btn-primary" href="<?php echo '/participants/create/'.$tournaments_item['id'] ?>" role="button">Participate</a>
        <?php } ?>
        <a class="btn btn-primary" href="<?php echo '/matches/index/'.$tournaments_item['id'] ?>" role="button" target="_blank">Matches</a>
        
        <?php if (isset($_SESSION['logged_user']) && $_SESSION['logged_user']['id'] === $tournaments_item['manager_id'] ) { ?>
        <a class="btn btn-success" href="<?php echo '/tournaments/edit/'.$tournaments_item['id'] ?>" role="button">Edit Tournaments</a>
        <a class="btn btn-success" href="<?php echo '/participants/manage/'.$tournaments_item['id'] ?>" role="button" target="_blank">Manage Participants</a>
        <a class="btn btn-success" href="<?php echo '/matches/manage/'.$tournaments_item['id'] ?>" role="button" target="_blank">Manage Matches</a>
        <?php } ?>
    </div>
</div>

<div class="profile-extra">
    <ul class="nav nav-tabs">
        <li class="active" role="presentation">
            <a href="#info" aria-controls="info" role="tab" data-toggle="tab">Info</a>
        </li>
        <li role="presentation">
            <a href="#standings" aria-controls="standings" role="tab" data-toggle="tab">Standings</a>
        </li>
    </ul>

    <div class="tab-content">
        <!-- Tournament Info -->
        <div role="tabpanel" class="tab-pane active" id="info">
            <?php if ($tournaments_item['youtube_channel_id'] != NULL) { ?>
            <p style="text-align:center; margin:10px;"><iframe width="560" height="315" src="https://www.youtube.com/embed/live_stream?channel=<?php echo $tournaments_item['youtube_channel_id'] ?>&autoplay=1" frameborder="0" allowfullscreen></iframe></p>
            <?php } ?>
            <p><?php echo nl2br($tournaments_item['description']) ?></p>
        </div>
        <!-- Tournament Standings -->
        <div role="tabpanel" class="tab-pane" id="standings">
            <center><h3>Current Standings</h3></center>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Rank</th>
                        <th>Name</th>
                        <th>Points</th>
                        <th>MWP</th>
                        <th>OMW</th>
                        <th>PGW</th>
                        <th>OGW</th>
                    </tr>    
                </thead>
                <tbody>
                    <?php $rank = 1 ?>
                    <?php foreach ($participants as $partic) : ?>
                    <tr>
                        <?php if ($partic['name'] != 'Dummy') { ?>
                        <td><?php echo $rank++ ?></td>
                        <td><?php echo $partic['name'] ?></td>
                        <td>
                        <?php
                        if ($tournaments_item['type'] == 1) {
                            echo $partic['points'];
                        } else if ($tournaments_item['type'] == 2) {
                            if ($partic['points'] >= 0) {
                                echo 'IN';
                            } else {
                                echo 'OUT';
                            }
                        }
                        ?>
                        </td>
                        <td><?php echo $partic['mwp'] ?></td>
                        <td><?php echo $partic['omw'] ?></td>
                        <td><?php echo $partic['pgw'] ?></td>
                        <td><?php echo $partic['ogw'] ?></td>
                        <?php } ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>    
            </table>
        </div>
    </div>
</div>