<li class="dropdown navbar-item">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <span class="glyphicon glyphicon-log-in"></span><br />
        Login <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li class="login-dropdown">
            <?php echo form_open('login/', 'class="form" enctype="multipart/form-data"'); ?>
                <div class="form-group">
                    <label class="sr-only" for="email_login">Email address</label>
                    <input type="email" name="email_login" class="form-control" placeholder="Email address" required />
                </div>
                <div class="form-group">
                    <label class="sr-only" for="password_login">Password</label>
                    <input type="password" name="password_login" class="form-control" placeholder="Password" required />
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                    <a href="/users/create" class="btn btn-primary btn-block">Register</a>
                </div>
            </form>
        </li>
    </ul>
</li>