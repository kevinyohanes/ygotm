<li class="navbar-item">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <span class="glyphicon glyphicon-user"></span><br />
        Welcome, <?php echo $_SESSION['logged_user']['name'] ?>
    </a>
    <ul class="dropdown-menu dropdown-default">
        <li><a href="/users/view/<?php echo $_SESSION['logged_user']['id'] ?>"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
        <li><a href="/users/edit/<?php echo $_SESSION['logged_user']['id'] ?>"><span class="glyphicon glyphicon-cog"></span> Preferences</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="/tournaments/manage/<?php echo $_SESSION['logged_user']['id'] ?>"><span class="glyphicon glyphicon-tower"></span> Your Tournaments</a></li>
        <?php if ($_SESSION['logged_user']['type'] == 0 || $_SESSION['logged_user']['type'] == 3) { ?>
        <li><a href="/articles/manage/<?php echo $_SESSION['logged_user']['id'] ?>"><span class="glyphicon glyphicon-book"></span> Your Articles</a></li>
        <?php } ?>
        <li><a href="/login/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
</li>