<?php
if (!isset($_SESSION)) {
    session_start();
}
$this->session->set_userdata('referer', current_url());

//var_dump($_SESSION['logged_user']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo 'YIC Today : '.$title; ?></title>
    <!-- Meta Data -->
    <meta property="og:url"           content="<?php echo base_url("$_SERVER[REQUEST_URI]"); ?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="<?php echo 'YIC Today : '.$title; ?>" />
    <meta property="og:description"   content="Yu-Gi-Oh! Tournament Management Software" />
    <!-- jQuery -->
    <link href="/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Bootstrap Core Files -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- Bootstrap Override -->
    <link href="/bootstrap/css/override.css" rel="stylesheet">
    <script src="/js/site.js"></script>
    <!-- reCAPTCHA -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- Google AdSense -->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-2406121601608173",
        enable_page_level_ads: true
    });
    </script>
</head>

<body>
    <div class="header-container">
        <nav class="navbar navbar-default">
            <ul class="navbar-left" role="navigation">
                <li class="navbar-item">
                    <a href="/">
                        <span class="glyphicon glyphicon-home"></span><br />
                        Home
                    </a>
                </li>
                <!-- Tournament Dropdown -->
                <li class="navbar-item">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-fire"></span><br />
                        Duel Arena
                    </a>
                    <ul class="dropdown-menu dropdown-default">
                        <li><a href="/tournaments/"><span class="glyphicon glyphicon-tower"></span> Tournaments</a></li>
                        <li><a href="/tournaments/past"><span class="glyphicon glyphicon-tower"></span> Past Tournaments</a></li>
                    </ul>
                </li>
                <!-- Article Dropdown -->
                <li class="navbar-item">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-file"></span><br />
                        Articles
                    </a>
                    <ul class="dropdown-menu dropdown-default">
                        <li><a href="/articles/"><span class="glyphicon glyphicon-book"></span> All Articles</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="navbar-right" role="navigation">
                <!-- Help Dropdown -->
                <li class="navbar-item">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-question-sign"></span><br />
                        Help &amp; Info
                    </a>
                    <ul class="dropdown-menu dropdown-default">
                        <li><a href="/faq"><span class="glyphicon glyphicon-question-sign"></span> FAQ</a></li>
                    </ul>
                </li>
                <?php
                if (!isset($_SESSION['logged_user'])) {
                    include('login_dropdown.php');
                } else {
                    include('user_dropdown.php');
                }
                ?>
            </ul>
        </nav>
        <a href="/" class="home-logo"><img src="/img/logo.png" /></a>
    </div>

    <div class="main-container">
        <div class="main-title">
            <h1><?php echo $title; ?><span class="glyphicon glyphicon-home pull-right"></span></h1>
            <div class="clearfix"></div>
        </div>

        <?php if ($this->session->flashdata('msg') !== NULL) { ?>
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
        <?php } ?>
        <div class="content-wrapper">