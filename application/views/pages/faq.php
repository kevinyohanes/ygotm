<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead1">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq1" aria-expanded="true" aria-controls="faq1">
                Apa itu "YIC Today"?
                </a>
            </h4>
        </div>

        <div id="faq1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="faqHead1">
            <div class="panel-body">
            YIC Today adalah aplikasi web yang memungkinkan komunitas Yu-Gi-Oh! Indonesia untuk mengorganisir turnamen; mulai dari registrasi peserta, pairing otomatis, hingga menghitung poin klasemen dari hasil pertandingan.
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead2">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq2" aria-expanded="true" aria-controls="faq2">
                Bagaimana saya dapat memulai untuk mengadakan turnamen?
                </a>
            </h4>
        </div>

        <div id="faq2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faqHead2">
            <div class="panel-body">
            Pertama, silahkan mendaftar di laman <a href="/users/create">YIC Today : User Registration</a> dan isi data diri anda dengan lengkap. Setelah selesai, anda akan diminta untuk mengkonfirmasi akun e-mail anda. <b>Catatan:</b> Ada kemungkinan e-mail verifikasi masuk ke dalam folder 'Spam' anda tanpa disengaja.
            <br><br>
            Kedua, setelah sukses melakukan proses verifikasi, anda bisa Login melalui menu dropdown di samping kanan logo YIC Today (atau di laman <a href="/login">YIC Today : Login</a>).
            <br><br>
            Sekarang setelah sukses login, anda bisa mengadakan turnamen anda sendiri melalui menu dropdown pengguna di samping kanan logo dan pilih 'Create a Tournament' (atau di laman <a href="/tournaments/create">YIC Today : New Tournament</a>)
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead3">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq3" aria-expanded="true" aria-controls="faq3">
                Bagaimana cara saya memasukkan peserta ke turnamen saya?
                </a>
            </h4>
        </div>

        <div id="faq3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faqHead3">
            <div class="panel-body">
            UPDATE 16/10/2017: Sekarang manajer turnamen bisa 'mengundang' calon peserta hanya dengan input nama dan alamat e-mail! Alamat e-mail nantinya akan dipergunakan untuk memberikan undangan turnamen via e-mail; segera datang di update selanjutnya!
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead4">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq4" aria-expanded="true" aria-controls="faq4">
                Bagaimana cara saya untuk berpartisipasi/ikut dalam suatu turnamen?
                </a>
            </h4>
        </div>

        <div id="faq4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faqHead4">
            <div class="panel-body">
            Calon peserta dapat mendaftar dengan membuka halaman turnamen yang dimaksud, lalu pilih 'Participate'. Anda akan diminta untuk memasukkan data diri. <b>Anda tidak mendaftar ke web YIC Today,</b> melainkan mendaftar untuk turnamen tersebut saja.
            <br><br>
            Anda dianjurkan memeriksa e-mail anda untuk memeriksa apakah terdapat pesan dari manajer turnamen, seperti cara pembayaran atau syarat lainnya. Data anda akan di-review oleh manajer sebelum anda diterima untuk mengikuti turnamen.
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead5">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq5" aria-expanded="true" aria-controls="faq5">
                Saya tidak ingin repot! Apakah saya bisa meminta langsung ke manajer turnamen untuk memasukkan nama saya?
                </a>
            </h4>
        </div>

        <div id="faq5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faqHead5">
            <div class="panel-body">
            UPDATE 16/10/2017: Sekarang manajer turnamen bisa 'mengundang' calon peserta hanya dengan input nama dan alamat e-mail! Alamat e-mail nantinya akan dipergunakan untuk memberikan undangan turnamen via e-mail; segera datang di update selanjutnya!
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead6">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq6" aria-expanded="true" aria-controls="faq6">
                Ada peserta yang sudah mendaftar di turnamen saya, tapi tidak terlihat?
                </a>
            </h4>
        </div>

        <div id="faq6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faqHead6">
            <div class="panel-body">
            Para peserta yang sudah mendaftar harus 'diterima' oleh manajer turnamen sebelum mereka dapat di-pairing saat turnamen dilaksanakan. Untuk 'menerima' peserta, manajer dapat login terlebih dahulu, kemudian buka halaman turnamen milik anda, dan pilih <b>Approve Participants</b>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead7">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq7" aria-expanded="true" aria-controls="faq7">
                Turnamen saya sudah siap dilaksanakan! Saya harus mulai darimana?
                </a>
            </h4>
        </div>

        <div id="faq7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faqHead7">
            <div class="panel-body">
            Untuk memulai pairing, manajer dapat membuka halaman turnamen yang dimaksud (dalam keadaan sudah login tentunya) dan pilih <b>Manage Tournaments</b>. Anda akan diberikan beberapa pilihan:
            <br><br>
            <b>Generate Matches</b> akan menghasilkan satu ronde pairing secara otomatis sesuai jenis turnamen (Swiss atau Elimination). Dianjurkan untuk menyelesaikan ronde satu per satu sebelum melakukan 'generate' ronde selanjutnya.
            <br><br>
            <b>Update Standings</b> akan memperbarui tabel klasemen secara manual yang ada di laman turnamen tersebut berdasarkan hasil pertandingan yang sudah ada. Normalnya, setiap kali melakukan <b>Generate Matches</b>, tabel akan diperbarui otomatis. Pilihan ini berguna jika terjadi kesalahan input skor dan perlu penyesuaian di tabel klasemen.
            <br><br>
            <b>RESET</b> akan MENGHAPUS dan MENGULANG SELURUH DATA PERTANDINGAN DAN KLASEMEN. Data peserta tidak akan terhapus. Semua resiko ditanggung oleh manajer turnamen tersebut.
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead8">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq8" aria-expanded="true" aria-controls="faq8">
                Kenapa ada peserta yang dipasangkan melawan 'Dummy'?
                </a>
            </h4>
        </div>

        <div id="faq8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faqHead8">
            <div class="panel-body">
            Jika jumlah peserta anda ganjil, satu atau beberapa peserta akan menerima 'bye' dan dipasangkan dengan peserta 'Dummy'. Untuk menjaga agar poin klasemen tetap teratur, peserta yang dipasangkan dengan 'Dummy' sebaiknya diberi skor menang mutlak (2-0).
            <br><br>
            Peserta yang mendapat 'bye' atau 'Dummy' mendapat poin penuh, namun tidak mendapat poin 'tiebreaker'.
            <br><br>
            Tidak perlu takut kalau mendapat 'bye' atau 'Dummy' membuat anda tidak bisa juara! Juara 1 Bekasi Open Super Series, Putu Eka Yuditha Syahputra berhasil finish di Top 8 Swiss walau mendapat 'bye', sebelum akhirnya memenangkan turnamen di ronde eliminasi.
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="faqHead9">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq9" aria-expanded="true" aria-controls="faq9">
                Bagaimana cara memasukkan skor pertandingan?
                </a>
            </h4>
        </div>

        <div id="faq9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faqHead9">
            <div class="panel-body">
            Di laman <b>Manage Tournaments</b>, cari pertandingan yang dimaksud, masukkan skor ke dalam kotak skor masing-masing pemain, dan KLIK SUBMIT. Skor pertandingan tidak akan tersimpan hingga anda memilih SUBMIT dan tombol menjadi terkunci.
            <br><br>
            Jika terjadi kesalahan input anda bisa memuat ulang (refresh) laman tersebut. Perbaiki skornya dan KLIK SUBMIT. Selalu periksa ulang skor pertandingan sebelum melakukan <b>Generate Matches</b> untuk menghindari pairing yang tidak adil.
            </div>
        </div>
    </div>
</div>

<p>(Updated on: 10 October 2017)</p>