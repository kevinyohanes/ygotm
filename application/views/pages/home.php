<div id="home_carousel" class="carousel slide home-carousel" data-ride="carousel" data-interval="5000">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php $i = 0; ?>
        <?php foreach ($banners as $b) : ?>
        <li data-target="#home_carousel" data-slide-to="<?php echo($i); ?>" class="<?php if ($i == 0) {echo('active');} ?>"></li>
        <?php $i += 1; ?>
        <?php endforeach; ?>
    </ol>
    
    <!-- Wrapper -->
    <div class="carousel-inner" role="listbox">
        <?php $i = 0; ?>
        <?php foreach ($banners as $b) : ?>
        <div class="item <?php if ($i == 0) {echo('active');} ?>">
            <a href="<?php echo($b['url']); ?>"><img src="<?php echo($b['img_path']); ?>" /></a>
        </div>
        <?php $i+= 1 ; ?>
        <?php endforeach; ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#home_carousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#home_carousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<hr>

<div class="home-container">
    <div class="home-sidebar">
        <h4><span class="glyphicon glyphicon-file"></span> Latest Post</h4>
        <div class="list-group">
            <?php foreach ($articles as $a) : ?>
            <a href="/articles/<?php echo $a['slug']; ?>" class="list-group-item"><?php echo $a['title']; ?></a>
            <?php endforeach; ?>
        </div>

        <h4><span class="glyphicon glyphicon-tower"></span> Upcoming Tournaments</h4>
        <div class="list-group">
            <?php foreach ($tournaments as $t) : ?>
            <a href="/tournaments/<?php echo $t['id']; ?>" class="list-group-item">
                <h4><?php echo date_format(date_create($t['start_time']), 'j F Y H:i:s') ?></h4>
                <?php echo '<b>'.$t['name'].'</b><br> '.$t['address'] ?>
            </a>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="home-content">
        <h1><?php echo $latest_article['title']; ?></h1>
        <p>
            by <?php echo $latest_article['writer'] ?><br>
            Last Update: <?php echo date_format(date_create($latest_article['time_modified']), 'j F Y H:i:s'); ?>
        </p>
        <?php echo $latest_article['content']; ?>
    </div>

    <div class="clearfix"></div>
</div>