<?php
echo validation_errors(
    '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
    '</div>'
    );
?>

<?php echo form_open('banners/create', 'class="form-horizontal" enctype="multipart/form-data"'); ?>
    <div class="form-group">
        <label for="url" class="col-sm-2 control-label">URL</label>
        <div class="col-sm-10">
            <input type="text" name="url" class="form-control" placeholder="https://www.google.co.id/" required />
        </div>
    </div>
    <div class="form-group">
        <label for="img_path" class="col-sm-2 control-label">Image Upload</label>
        <div class="col-sm-10">
            <img src="<?php echo $banners_item['img_path'] ?>" width="500" />
            <input type="file" name="img" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary btn-register">Submit</button>
        </div>
    </div>
</form>