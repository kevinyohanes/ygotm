<a href="/banners/create/" class="btn btn-success">New Banner</a>
<table class="table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>URL</th>
            <th>Image</th>
        </tr>    
    </thead>
    <tbody>
        <?php foreach ($banners as $banners_item) : ?>
        <tr>
            <td><?php echo $banners_item['id'] ?></td>
            <td><a href="<?php echo $banners_item['url'] ?>"><?php echo $banners_item['url'] ?></a></td>
            <td><img src="<?php echo $banners_item['img_path'] ?>" style="height: 100px;" /></td>
            <td><a href="/banners/edit/<?php echo $banners_item['id'] ?>" class="btn btn-primary">Edit</a></td>
            <td><a href="/banners/delete/<?php echo $banners_item['id'] ?>" class="btn btn-danger">Delete</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>    
</table>