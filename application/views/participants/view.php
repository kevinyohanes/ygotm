<?php
var_dump($opponents);
?>

<div class="profile-container row">
    <div class="profile-sidebar col-sm-3">
        <?php if ($participant['photo'] == NULL) { ?>
        <div class="profile-picture" style="background-image: url(/img/yugi.png)"></div>
        <?php } else { ?>
        <div class="profile-picture" style="background-image: url(<?php echo $participant['photo'] ?>)"></div>
        <?php } ?>
        <p>
            <?php echo $participant['name'] ?> <br />            
            <?php echo $participant['city'] ?>
        </p>
        <div class="profile-menu">
            <a href="mailto:<?php echo $participant['email'] ?>"><span class="glyphicon glyphicon-envelope"></span> Send Email</a>
            <a href="/participants/edit/<?php echo $participant['id'] ?>"><span class="glyphicon glyphicon-pencil"></span> Edit Participants</a>
        </div>
    </div>

    <div class="profile-main col-sm-9">
        <?php if($participant['tournament_logo'] == NULL) { ?>
        <center><img src="/img/logo.png" height="150" /></center>
        <?php } else { ?>
        <center><img src="<?php echo $participant['tournament_logo'] ?>" height="150" /></center>
        <?php } ?>
        <center><h3>Participant of <?php echo $participant['tournament_name'] ?></h3></center>

        <h3>Duelist Information</h3>
        <div class="general-info row">
            <div class="col-xs-3">Full Name</div>
            <div class="col-xs-9">: <?php echo $participant['name'] ?></div>
            <div class="col-xs-3">Email Address</div>
            <div class="col-xs-9">: <?php echo $participant['email'] ?></div>
            <div class="col-xs-3">Birthdate</div>
            <div class="col-xs-9">: <?php echo date('j F Y', strtotime($participant['birthdate'])) ?> <br /></div>
            <div class="col-xs-3">Address</div>
            <div class="col-xs-9">: <?php echo $participant['address'] ?></div>
            <div class="col-xs-3">City</div>
            <div class="col-xs-9">: <?php echo $participant['city'] ?></div>
            <div class="col-xs-3">Phone Number</div>
            <div class="col-xs-9">: <?php echo $participant['phone'] ?></div>
        </div>
    </div>
</div>