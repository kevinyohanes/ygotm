<a class="btn btn-primary" href="/tournaments/view/<?php echo $tournament['id'] ?>"> Go Back</a>

<hr>
<form id="quickPartic" class="form-inline">
    <div class="form-group">
        <label for="particName">Duelist Name</label>
        <input id="quickParticName" type="text" class="form-control" placeholder="Seto Kaiba" required>
    </div>
    <div class="form-group">
        <label for="emailAddress">Email</label>
        <input id="quickParticEmail" type="email" class="form-control" placeholder="seto@kaibacorp.net" required>
    </div>
    <input id="quickParticCity" type="hidden" value="<?php echo $tournament['city_id']; ?>">
    <button type="submit" class="btn btn-success">Add Duelist</button>
</form>
<hr>

<table class="table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email Address</th>
            <th>Change Approval</th>
            <th>1st Round Advantage</th>
        </tr>    
    </thead>
    <tbody>
        <?php foreach ($participants as $partic) : ?>
        <tr>
            <th><?php echo $partic['id'] ?></th>
            <td><?php echo $partic['name'] ?></td>
            <td><?php echo $partic['email'] ?></td>
            <td>
                <?php if ($partic['type'] == 1) { ?>
                <button id="<?php echo $partic['id'] ?>" class="btn btn-success approval-button"><span class="glyphicon glyphicon-ok"></span> Approve</button>
                <?php } else { ?>
                <button id="<?php echo $partic['id'] ?>" class="btn btn-danger approval-button"><span class="glyphicon glyphicon-remove"></span> Un-approve</button>
                <?php } ?>
            </td>
            <td>
                <?php if ($partic['advantage'] == FALSE) { ?>
                <button id="<?php echo $partic['id'] ?>" class="btn btn-success advantage-button"><span class="glyphicon glyphicon-ok"></span> Give</button>
                <?php } else { ?>
                <button id="<?php echo $partic['id'] ?>" class="btn btn-danger advantage-button"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                <?php } ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>    
</table>

<script type="text/javascript">
    // Update participants list asynchronously
    function updatePartic() {
        console.log('Start');
        var tournamentId = '<?php echo $tournament['id'] ?>';
        $.ajax({
            url: '<?php echo base_url("index.php/participants/index_ajax/") ?>'+tournamentId,
            type: 'GET',
            success: function (result) {
                $('.content-wrapper').html(result);
                console.log('Finish');
            }
        });
    }

    $(document).ready(function() {
        // Switch approval status and button with AJAX
        $(".approval-button").click(function() {
            var partic_id = $(this).attr("id");
            var target = '<?php echo base_url("index.php/participants/set_type/") ?>'+partic_id;
            var self = this;
            $.ajax({
                url: target,
                type: "POST",
                data: {partic_id: partic_id},
                success: function(response) {
                    console.log(response);
                    console.log(partic_id);
                    if (response == 2) {
                        $(self).removeClass("btn-success");
                        $(self).addClass("btn-danger");
                        $(self).html('<span class="glyphicon glyphicon-remove"></span> Un-approve');
                    } else if (response == 1) {
                        $(self).removeClass("btn-danger");
                        $(self).addClass("btn-success");
                        $(self).html('<span class="glyphicon glyphicon-ok"></span> Approve');
                    } else {
                        alert("Failed to do the operation.")
                    }
                },
                error: function() {
                    alert("Something went wrong. Please refresh the page before trying again.");
                }
            });
        });

        $(".advantage-button").click(function() {
            var partic_id = $(this).attr("id");
            var target = '<?php echo base_url("index.php/participants/set_advantage/") ?>'+partic_id;
            var self = this;
            $.ajax({
                url: target,
                type: "POST",
                data: {partic_id: partic_id},
                success: function (response) {
                    console.log(response);
                    console.log(partic_id);
                    if (response == 'given') {
                        $(self).removeClass("btn-success");
                        $(self).addClass("btn-danger");
                        $(self).html('<span class="glyphicon glyphicon-remove"></span> Cancel');
                    } else if (response == 'cancelled') {
                        $(self).removeClass("btn-danger");
                        $(self).addClass("btn-success");
                        $(self).html('<span class="glyphicon glyphicon-ok"></span> Give');
                    }
                },
                error: function() {
                    alert("Something went wrong. Please refresh the page before trying again.");
                }
            });
        });

        // Quick Submit New Players
        $('#quickPartic').submit(function(event) {
            var name = $('#quickParticName').val();
            var email = $('#quickParticEmail').val();
            var city = $('#quickParticCity').val();
            var tournamentId = <?php echo $tournament['id'] ?>;
            $.ajax({
                url: window.location.origin+"/participants/quick_create/"+tournamentId,
                type: 'POST',
                data: {name: name, email: email, city: city},
                success: function () {
                    updatePartic();
                },
                error: function () {
                    alert('Something went wrong. Please refresh the web page.');
                }
            });
            return false;
        });
    });
</script>