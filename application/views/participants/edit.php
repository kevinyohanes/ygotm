<?php
echo validation_errors(
    '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
    '</div>'
    );
?>

<?php echo form_open('participants/edit/'.$participant['id'], 'class="form-horizontal" enctype="multipart/form-data"'); ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" placeholder="John Doe" required value="<?php echo $participant['name'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">Birthdate</label>
        <div class="col-sm-10">
            <input type="date" name="birthdate" class="form-control" required value="<?php echo $participant['birthdate'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="personal_number" class="col-sm-2 control-label">Personal Number</label>
        <div class="col-sm-10">
            <input type="text" name="personal_number" class="form-control" placeholder="KTP/SIM (Optional)" value="<?php echo $participant['personal_number'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="phone" class="col-sm-2 control-label">Phone</label>
        <div class="col-sm-10">
            <input type="text" name="phone" class="form-control" placeholder="08**********" required value="<?php echo $participant['phone'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Address</label>
        <div class="col-sm-10">
            <textarea name="address" rows="3" class="form-control" ><?php echo $participant['address'] ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="city" class="col-sm-2 control-label">City</label>
        <div class="col-sm-10">
            <select class="form-control" name="city">
                <?php foreach ($cities as $city) : ?>
                <option value="<?php echo($city['id']); ?>" <?php if ($city['name'] == $participant['city']) {echo 'selected';} ?>><?php echo($city['name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-10">
            <img src="<?php echo $participant['photo'] ?>" width="150" />
            <input type="file" name="photo" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary btn-register">Save</button>
            <a href="/participants/<?php echo $participant['id'] ?>" type="submit" class="btn btn-register">Cancel</a>
        </div>
    </div>
</form>