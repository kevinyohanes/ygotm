<?php
echo validation_errors(
    '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
    '</div>'
    );
?>

<?php echo form_open('participants/create/'.$tournament_id, 'class="form-horizontal" enctype="multipart/form-data"'); ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" placeholder="John Doe" required />
        </div>
    </div>
    <div class="form-group">
        <label for="birthdate" class="col-sm-2 control-label">Birthdate</label>
        <div class="col-sm-10">
            <input type="date" name="birthdate" class="form-control" required />
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" class="form-control" placeholder="johndoe@email.com" required />
        </div>
    </div>
    <div class="form-group">
        <label for="personal_number" class="col-sm-2 control-label">Personal Number</label>
        <div class="col-sm-10">
            <input type="text" name="personal_number" class="form-control" placeholder="KTP/SIM (Optional)" />
        </div>
    </div>
    <div class="form-group">
        <label for="phone" class="col-sm-2 control-label">Phone</label>
        <div class="col-sm-10">
            <input type="text" name="phone" class="form-control" placeholder="08**********" required />
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Address</label>
        <div class="col-sm-10">
            <textarea name="address" rows="3" class="form-control" ></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="city" class="col-sm-2 control-label">City</label>
        <div class="col-sm-10">
            <select class="form-control" name="city">
                <?php foreach ($cities as $city) : ?>
                <option value="<?php echo($city['id']); ?>"><?php echo($city['name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary btn-register">Register</button>
        </div>
    </div>
</form>