<script src="https://use.fontawesome.com/ab07705fb7.js"></script>
<p>
by <?php echo $articles_item['writer'] ?><br>
Last Update: <?php echo date_format(date_create($articles_item['time_modified']), 'j F Y H:i:s'); ?><br>
<a class="btn btn-primary" href="https://www.facebook.com/sharer.php?u=<?php echo urlencode(base_url("$_SERVER[REQUEST_URI]")) ?>" target="_blank" style="background-color: #4267b2;">
    <i class="fa fa-facebook"></i> Share
</a>
<a class="btn btn-primary" href="https://twitter.com/share?text=<?php echo urlencode($title) ?>&url=<?php echo urlencode(base_url("$_SERVER[REQUEST_URI]")) ?>" target="_blank" style="background-color: #1da1f2;">
    <i class="fa fa-twitter"></i> Share
</a>
</p>

<?php echo $articles_item['content']; ?>