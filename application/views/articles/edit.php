<?php
echo validation_errors(
    '<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
    '</div>'
    );
?>

<link href="/plugins/summernote/summernote.css" rel="stylesheet">
<script src="/plugins/summernote/summernote.js"></script>

<?php echo form_open('articles/edit/'.$articles_item['id'], 'id="articles_form" class="form-horizontal" style="display: none;" enctype="multipart/form-data"'); ?>
    <div class="form-group">
        <label for="title" class="col-sm-2 control-label">Title</label>
        <div class="col-sm-10">
            <input type="text" name="title" class="form-control" placeholder="John Doe" value="<?php echo $articles_item['title'] ?>" required />
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Content</label>
        <div class="col-sm-10">
            <div id="content"><?php echo $articles_item['content'] ?></div>
        </div>
    </div>
    <textarea name="content" id="html_content" style="display: none;"><?php echo $articles_item['content'] ?></textarea>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button name="save_draft" class="btn btn-success btn-register">Save Draft</button>
            <button name="save_publish" class="btn btn-primary btn-register">Save &amp; Publish</button>
        </div>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function () {
    var filename;
    $("#articles_form").show();

    $("#content").summernote({
        height: 300,
        fontNames: ['Matrix Book'],
        fontNamesIgnoreCheck: ['Matrix Book'],
        callbacks: {
            onImageUpload: function (files, editor, $editable) {
                sendFile(files[0], editor, $editable);
            },
            onMediaDelete: function ($target, editor, $editable) {
                filename = $target[0].dataset.filename;
                deleteFile(filename);
                $target.remove();
            }
        }
    });

    $("#content").summernote('fontName', 'Matrix Book');
    //$("#content").summernote('pasteHTML', $("#html_content").html());

    function getContent() {
        var content = $("#content").summernote("code");
        $('#html_content').html(content);
        console.log(content);
    }

    $("#content").on('summernote.change', getContent);
});
</script>