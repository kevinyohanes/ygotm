<ul class="articles-list">
    <?php foreach ($articles as $articles_item) : ?>
    <li class="articles-list-item">
        <a href="/articles/view/<?php echo $articles_item['slug']; ?>">
            <h1><?php echo $articles_item['title'] ?></h1>
            <h4>by <?php echo $articles_item['writer'] ?></h4>
            <h4><?php echo date_format(date_create($articles_item['time_created']), 'j F Y'); ?></h4>
            <?php
            $string = strip_tags($articles_item['content']);
            if (strlen($string) > 500) {
                $string = wordwrap($string, 500);
                $string = substr($string, 0, strpos($string, "\n"));
                $string .= '...<b>(Continue Reading)</b>';
            }
            ?>
            <p><?php echo $string; ?></p>
        </a>
    </li>
    <?php endforeach; ?>
</ul>