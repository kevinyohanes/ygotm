<a href="/articles/create/" class="btn btn-primary">
    <i class="glyphicon glyphicon-pencil"></i> Write New Articles
</a>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Title</th>
            <th>Published</th>
            <th>Time Created</th>
            <th>Last Modified</th>
        </tr>    
    </thead>
    <tbody>
        <?php foreach ($articles as $articles_item) : ?>
        <tr>
            <td><?php echo $articles_item['title'] ?></td>
            <td><?php echo ($articles_item['published'] ? 'Yes' : 'No') ?></td>
            <td><?php echo date_format(date_create($articles_item['time_created']), 'j F Y - H:i') ?></td>
            <td><?php echo date_format(date_create($articles_item['time_modified']), 'j F Y - H:i') ?></td>
            <td><a href="/articles/<?php echo $articles_item['slug'] ?>" class="btn btn-success">View</a></td>
            <td><a href="/articles/edit/<?php echo $articles_item['id'] ?>" class="btn btn-primary">Edit</a></td>
            <td><a href="/articles/delete/<?php echo $articles_item['id'] ?>" class="btn btn-danger">Delete</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>    
</table>