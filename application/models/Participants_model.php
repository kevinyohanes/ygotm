<?php
class Participants_model extends CI_Model {
    
    public function __construct() {
        $this->load->database();
        $this->load->model('matches_model');
    }

    public function get_participants($partic_id = NULL, $tournament_id = NULL) {
        if ($partic_id == NULL && $tournament_id != NULL) {
            $query = $this->db->select('id, name, email, type, advantage')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->get();
            return $query->result_array();
        } else if ($partic_id != NULL) {
            $query = $this->db->select(
                'participants.*,
                cities.name AS city,
                tournaments.id AS tournament_id,
                tournaments.name AS tournament_name,
                tournaments.logo AS tournament_logo'
            )
            ->from('participants')
            ->join('cities', 'cities.id = participants.city_id')
            ->join('tournaments', 'tournaments.id = participants.tournament_id')
            ->where('participants.id', $partic_id)
            ->get();
            return $query->row_array();
        }

        return FALSE;
    }

    public function get_unapproved_participants($tournament_id = NULL) {
        if ($tournament_id != NULL) {
            $query = $this->db->select('id, name, email, type')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 1)
            ->get();
            return $query->result_array();
        }

        return FALSE;
    }

    public function get_approved_participants($tournament_id = NULL, $condition = 'all') {
        if ($tournament_id != NULL && $condition == 'all') {
            // 'all' => get ALL approved participants
            $query = $this->db->select('id, name, email, type, advantage, points, mwp, omw, pgw, ogw')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->order_by('points DESC, mwp DESC, omw DESC, pgw DESC, ogw DESC')
            ->get();
            return $query->result_array();
        } else if ($tournament_id != NULL && $condition == 'adv') {
            // 'adv' => get ADVANTAGE approved participants
            $query = $this->db->select('id, name, email, type, advantage, points, mwp, omw, pgw, ogw')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->where('advantage', 1)
            ->order_by('points DESC, mwp DESC, omw DESC, pgw DESC, ogw DESC')
            ->get();
            return $query->result_array();
        } else if ($tournament_id != NULL && $condition == 'non') {
            // 'non' => get NON-ADVANTAGE approved participants
            $query = $this->db->select('id, name, email, type, advantage, points, mwp, omw, pgw, ogw')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->where('advantage', 0)
            ->order_by('points DESC, mwp DESC, omw DESC, pgw DESC, ogw DESC')
            ->get();
            return $query->result_array();
        }

        return FALSE;
    }

    public function count_approved_participants($tournament_id = NULL, $condition = 'all') {
        if ($tournament_id != NULL && $condition == 'all') {
            // 'all' => get ALL approved participants
            $query = $this->db->select('id')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->get();
            return $query->num_rows();
        } else if ($tournament_id != NULL && $condition == 'adv') {
            // 'adv' => get ADVANTAGE approved participants
            $query = $this->db->select('id')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->where('advantage', 1)
            ->get();
            return $query->num_rows();
        } else if ($tournament_id != NULL && $condition == 'non') {
            // 'non' => get NON-ADVANTAGE approved participants
            $query = $this->db->select('id')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->where('advantage', 0)
            ->get();
            return $query->num_rows();
        }

        return FALSE;
    }

    public function get_participants_ids($tournament_id = NULL) {
        if ($tournament_id != NULL) {
            $query = $this->db->select('id')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->order_by('points DESC, mwp DESC, omw DESC, pgw DESC, ogw DESC')
            ->get();
            foreach ($query->result() as $p) {
                $data[] = $p->id;
            }
            return array_unique($data);
        }

        return FALSE;
    }

    public function get_participants_grouped($tournament_id = NULL) {
        if ($tournament_id != NULL) {
            $query = $this->db->select('*')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->order_by('points DESC, mwp DESC, omw DESC, pgw DESC, ogw DESC')
            ->get();
            $arr = $query->result_array();
            $data = array();
            foreach ($arr as $a) {
                $group = $a['points'];
                $data[$group][] = $a['id'];
            }
            return array_values($data);
        }
        return FALSE;
    }

    public function get_participants_elim($tournament_id = NULL) {
        if ($tournament_id != NULL) {
            $query = $this->db->select('id, name, email, type, advantage, points, mwp, omw, pgw, ogw')
            ->from('participants')
            ->where('tournament_id', $tournament_id)
            ->where('type', 2)
            ->where('points >=', 0)
            ->order_by('points DESC, mwp DESC, omw DESC, pgw DESC, ogw DESC')
            ->get();
            return $query->result_array();
        }

        return FALSE;
    }

    public function quick_set_participants($tournament_id) {
        $data = array(
            'email' => $this->input->post('email'),
            'name' => $this->input->post('name'),
            'birthdate' => date('Y-m-d H:i:s'),
            'phone' => '080000000000',
            'city_id' => $this->input->post('city'),
            'tournament_id' => $tournament_id,
            'type' => 2,
            'time_created' => date('Y-m-d H:i:s')
        );
        return $this->db->insert('participants', $data);
    }

    public function set_participants($tournament_id) {
        $data = array(
            'email' => $this->input->post('email'),
            'name' => $this->input->post('name'),
            'birthdate' => $this->input->post('birthdate'),
            'personal_number' => $this->input->post('personal_number'),
            'phone' => $this->input->post('phone'),
            'address' => $this->input->post('address'),
            'city_id' => $this->input->post('city'),
            'tournament_id' => $tournament_id,
            'time_created' => date('Y-m-d H:i:s')
        );

        return $this->db->insert('participants', $data);
    }

    public function set_dummy_participants($tournament_id) {
        if ($tournament_id == NULL) {
            return FALSE;
        }

        $data = array(
            'email' => 'dummy@dummy.com',
            'name' => 'Dummy',
            'birthdate' => date('Y-m-d'),
            'phone' => '081234567890',
            'city_id' => 1,
            'tournament_id' => $tournament_id,
            'type' => 2,
            'time_created' => date('Y-m-d H:i:s')
        );

        return $this->db->insert('participants', $data);
    }

    public function get_dummy_id($tournament_id) {
        if ($tournament_id == NULL) {
            return FALSE;
        }

        $query = $this->db->select('id')
        ->from('participants')
        ->where('tournament_id', $tournament_id)
        ->where('name', 'Dummy')
        ->get();

        $row = $query->row();
        
        if (isset($row)) {
            return $row->id;
        } else {
            return NULL;
        }
    }

    public function update_participants($partic_id, $photo_url = NULL) {
        if ($partic_id == NULL) {
            return FALSE;
        }

        $data = array(
            'name' => $this->input->post('name'),
            'birthdate' => $this->input->post('birthdate'),
            'personal_number' => $this->input->post('personal_number'),
            'phone' => $this->input->post('phone'),
            'photo' => $photo_url,
            'address' => $this->input->post('address'),
            'city_id' => $this->input->post('city')
        );

        $this->db->where('id', $partic_id);
        return $this->db->update('participants', $data);
    }

    public function set_partic_type($id) {
        $get_query = $this->db->select('id, type')->from('participants')->where('id', $id)->get();
        $row = $get_query->row_array();
        if (empty($row)){
            return FALSE;
        }

        if ($row['type'] == 1) {
            $this->db->where('id', $id);
            $this->db->update('participants', array('type' => 2, 'time_approved' => date('Y-m-d H:i:s')));
            return 2;
        } else if($row['type'] == 2) {
            $this->db->where('id', $id);
            $this->db->update('participants', array('type' => 1, 'time_approved' => NULL));
            return 1;
        } else {
            return FALSE;
        }
    }

    public function set_partic_advantage($id) {
        $get_query = $this->db->select('id, advantage')->from('participants')->where('id', $id)->get();
        $row = $get_query->row_array();
        if (empty($row)) {
            return FALSE;
        }

        if ($row['advantage'] == 0) {
            $this->db->where('id', $id);
            $this->db->update('participants', array('advantage' => 1, 'time_modified' => date('Y-m-d H:i:s')));
            return 'given';
        } else if ($row['advantage'] == 1) {
            $this->db->where('id', $id);
            $this->db->update('participants', array('advantage' => 0, 'time_modified' => date('Y-m-d H:i:s')));
            return 'cancelled';
        } else {
            return FALSE;
        }
    }

    public function set_points($partic_id) {
        $query = $this->db->select('matches.*')
        ->from('matches')
        ->where('partic1_id', $partic_id)
        ->or_where('partic2_id', $partic_id)
        ->get();
        $total = 0;
        foreach ($query->result() as $match) {
            if ($match->partic1_score == $match->partic2_score) {
                $total += 1;
            } else if ($match->partic1_id == $partic_id) {
                if ($match->partic1_score > $match->partic2_score) {$total += 3;}
            } else if ($match->partic2_id == $partic_id) {
                if ($match->partic1_score < $match->partic2_score) {$total += 3;}
            }
        }
        $this->db->where('id', $partic_id);
        return $this->db->update('participants', array('points' => $total));
    }

    public function set_points_elim($partic_id) {
        $query = $this->db->select('matches.*')
        ->from('matches')
        ->where('partic1_id', $partic_id)
        ->or_where('partic2_id', $partic_id)
        ->get();
        $point = 0;
        foreach ($query->result() as $match) {
            if (($match->partic1_id == $partic_id && $match->partic1_score < $match->partic2_score) || (($match->partic2_id == $partic_id && $match->partic2_score < $match->partic1_score))) {
                $point = -1;
                break;
            }
        }
        $this->db->where('id', $partic_id);
        return $this->db->update('participants', array('points' => $point));
    }

    public function set_mwp($partic_id) {
        $query = $this->db->select('matches.*')
        ->from('matches')
        ->where('partic1_id', $partic_id)
        ->or_where('partic2_id', $partic_id)
        ->get();
        $match_wins = 0;
        $total = 0;
        $tournament_id = $query->row()->tournament_id;
        $dummy = $this->get_dummy_id($tournament_id);
        foreach ($query->result() as $match) {
            // Exclude Dummy accounts
            if ($match->partic1_id == $dummy || $match->partic2_id == $dummy) {
                // Do nothing
            } else if ($match->partic1_id == $partic_id) {
                if ($match->partic1_score > $match->partic2_score) {$match_wins++;}
            } else if ($match->partic2_id == $partic_id) {
                if ($match->partic1_score < $match->partic2_score) {$match_wins++;}
            }
            $total++;
        }
        $mwp = ($match_wins / $total) * 100;
        $this->db->where('id', $partic_id);
        return $this->db->update('participants', array('mwp' => $mwp));
    }

    public function set_omw($partic_id) {
        $opponents = $this->matches_model->previous_opponents($partic_id);
        $query = $this->db->select('participants.name, participants.mwp')->from('participants')->where_in('id', $opponents)->get();
        $total = 0;
        foreach ($query->result() as $opp) {
            if ($opp->name == 'Dummy') {
                // Do Nothing
            } else {
                $total += $opp->mwp;
            }
        }
        $omw = ($total / count($opponents));
        $this->db->where('id', $partic_id);
        return $this->db->update('participants', array('omw' => $omw));
    }

    public function set_pgw($partic_id) {
        $query = $this->db->select('matches.*')
        ->from('matches')
        ->where('partic1_id', $partic_id)
        ->or_where('partic2_id', $partic_id)
        ->get();
        $game_wins = 0;
        $total = 0;
        $tournament_id = $query->row()->tournament_id;
        $dummy = $this->get_dummy_id($tournament_id);
        foreach($query->result() as $match) {
            if ($match->partic1_id == $dummy || $match->partic2_id == $dummy) {
                // Do nothing
            } else if ($match->partic1_id == $partic_id) {
                $game_wins += $match->partic1_score;
            } else if ($match->partic2_id == $partic_id) {
                $game_wins += $match->partic2_score;
            }
            $total += $match->partic1_score;
            $total += $match->partic2_score;
        }
        $pgw = ($game_wins / $total) * 100;
        $this->db->where('id', $partic_id);
        return $this->db->update('participants', array('pgw' => $pgw));
    }
    
    public function set_ogw($partic_id) {
        $opponents = $this->matches_model->previous_opponents($partic_id);
        $query = $this->db->select('participants.name, participants.pgw')->from('participants')->where_in('id', $opponents)->get();
        $total = 0;
        foreach($query->result() as $opp) {
            if ($opp->name == 'Dummy') {
                // Do Nothing
            } else {
                $total += $opp->pgw;
            }
        }
        $ogw = ($total / count($opponents));
        $this->db->where('id', $partic_id);
        return $this->db->update('participants', array('ogw' => $ogw));
    }

    public function reset_standing($tournament_id) {
        if ($tournament_id == NULL) {
            return FALSE;
        }

        $data = array(
            'points' => 0,
            'mwp' => 0.00,
            'omw' => 0.00,
            'pgw' => 0.00,
            'ogw' => 0.00
        );

        $this->db->where('tournament_id', $tournament_id);
        return $this->db->update('participants', $data);
    }

    public function remove_dummy($tournament_id) {
        if ($tournament_id == NULL) {
            return FALSE;
        }

        $this->db->where('tournament_id', $tournament_id)
        ->where('name', 'Dummy');
        return $this->db->delete('participants');
    }

    public function unapprove_dummy($tournament_id) {
        if ($tournament_id == NULL) {
            return FALSE;
        }

        $this->db->where('tournament_id', $tournament_id)
        ->where('name', 'Dummy');
        return $this->db->update('participants', array('type' => 1));
    }
}
?>