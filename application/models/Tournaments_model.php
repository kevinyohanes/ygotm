<?php
class Tournaments_model extends CI_Model {
    
    public function __construct() {
        $this->load->database();
    }

    public function get_tournaments($id = FALSE) {
        if ($id == FALSE) {
            // Select All Tournaments, sorted by earliest start date
            $query = $this->db->select(
                'tournaments.id,
                tournaments.name,
                tournaments.max_partic,
                tournaments.type,
                tournaments.start_time,
                tournaments.address,
                cities.name AS city,
                users.name AS manager')
            ->from('tournaments')
            ->join('cities', 'cities.id = tournaments.city_id')
            ->join('users', 'users.id = tournaments.manager_id')
            ->where('tournaments.start_time >', date("Y-m-d H:i:s"))
            ->order_by('tournaments.start_time DESC')
            ->get();
            return $query->result_array();
        }
        
        // Otherwise, get one tournament based on ID
        $query = $this->db->select('tournaments.*, cities.name AS city, users.name AS manager, users.photo AS manager_photo')
        ->from('tournaments')
        ->join('cities', 'cities.id = tournaments.city_id')
        ->join('users', 'users.id = tournaments.manager_id')
        ->where('tournaments.id', $id)
        ->get();
        return $query->row_array();
    }

    public function get_user_tournaments($user = NULL) {
        if ($user == NULL) {
            return FALSE;
        }

        $query = $this->db->select(
            'tournaments.id,
            tournaments.name,
            tournaments.max_partic,
            tournaments.type,
            tournaments.start_time,
            tournaments.address')
        ->from('tournaments')
        ->where('tournaments.manager_id', $user)
        ->order_by('tournaments.time_created')
        ->get();
        return $query->result_array();
    }

    public function get_past_tournaments() {
        $query = $this->db->select(
            'tournaments.id,
            tournaments.name,
            tournaments.max_partic,
            tournaments.type,
            tournaments.start_time,
            tournaments.address,
            cities.name AS city,
            users.name AS manager')
        ->from('tournaments')
        ->join('cities', 'cities.id = tournaments.city_id')
        ->join('users', 'users.id = tournaments.manager_id')
        ->order_by('tournaments.start_time DESC')
        ->get();
        return $query->result_array();
    }

    public function set_tournaments($user = NULL) {
        if ($user == NULL) {
            return FALSE;
        }

        $data = array(
            'name' => $this->input->post('name'),
            'type' => $this->input->post('type'),
            'start_time' => $this->input->post('start_time'),
            'end_time' => $this->input->post('end_time'),
            'max_partic' => $this->input->post('max_partic'),
            'max_rounds' => $this->input->post('max_rounds'),
            'address' => $this->input->post('address'),
            'youtube_channel_id' => $this->input->post('youtube_channel_id'),
            'city_id' => $this->input->post('city'),
            'description' => $this->input->post('description'),
            'message' => $this->input->post('message'),
            'manager_id' => $user,
            'time_created' => date('Y-m-d H:i:s')
        );

        return $this->db->insert('tournaments', $data);
    }

    public function update_tournaments($id = FALSE, $logo_url = NULL) {
        if ($id == FALSE) {
            return FALSE;
        }

        $data = array(
            'name' => $this->input->post('name'),
            'type' => $this->input->post('type'),
            'start_time' => $this->input->post('start_time'),
            'end_time' => $this->input->post('end_time'),
            'max_partic' => $this->input->post('max_partic'),
            'max_rounds' => $this->input->post('max_rounds'),
            'address' => $this->input->post('address'),
            'youtube_channel_id' => $this->input->post('youtube_channel_id'),
            'city_id' => $this->input->post('city'),
            'description' => $this->input->post('description'),
            'message' => $this->input->post('message'),
            'logo' => $logo_url
        );

        $this->db->where('id', $id);
        return $this->db->update('tournaments', $data);
    }

    public function inc_current_rounds($id = NULL) {
        if ($id == NULL) {
            return FALSE;
        }

        $this->db->where('id', $id);
        $this->db->set('current_rounds', 'current_rounds + 1', FALSE);
        return $this->db->update('tournaments');
    }

    public function close_registration($id) {
        if ($id == NULL) {
            return FALSE;
        }

        $this->db->where('id', $id);
        $this->db->set('open', FALSE);
        return $this->db->update('tournaments');
    }

    public function reset_tournament($id) {
        if ($id == NULL) {
            return FALSE;
        }

        $this->db->where('id', $id);
        $this->db->set('current_rounds', 0);
        $this->db->set('open', TRUE);
        return $this->db->update('tournaments');
    }
}
?>