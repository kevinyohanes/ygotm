<?php
class Banners_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_banners($id = NULL) {
        if ($id == NULL) {
            $query = $this->db->select('*')
            ->from('banners')
            ->get();
            
            return $query->result_array();
        }

        $query = $this->db->select('*')
        ->from('banners')
        ->where('id', $id)
        ->get();
        
        return $query->row_array();
    }

    public function set_banners() {
        $data = array('url' => $this->input->post('url'));
        $this->db->insert('banners', $data);
        return $this->db->insert_id();
    }

    public function update_banners($id = NULL, $img_path = NULL) {
        if ($id == NULL) {
            return FALSE;
        }

        if ($img_path == NULL) {
            $data = array('url' => $this->input->post('url'));
    
            $this->db->where('id', $id);
            return $this->db->update('banners', $data);
        }

        $data = array(
            'url' => $this->input->post('url'),
            'img_path' => $img_path
        );

        $this->db->where('id', $id);
        return $this->db->update('banners', $data);
    }

    public function delete_banners($id = NULL) {
        if ($id == NULL) {
            return FALSE;
        }
        $this->db->where('id', $id);
        $this->db->delete('banners');
    }
}
?>