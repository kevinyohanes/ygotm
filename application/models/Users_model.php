<?php
class Users_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_users($id = FALSE) {
        if ($id == FALSE) {
            $query = $this->db->select('users.id, users.name AS username, cities.name AS city')
            ->from('users')
            ->join('cities', 'cities.id = users.city_id')
            ->get();
            return $query->result_array();
        }

        $query = $this->db->select('users.*, cities.name AS city')
        ->from('users')
        ->join('cities', 'cities.id = users.city_id')
        ->where('users.id', $id)
        ->get();
        return $query->row_array();
    }

    public function set_users() {
        $this->load->helper('hasher');

        $pw_hash = generate_hash($this->input->post('password'));

        $data = array(
            'email' => $this->input->post('email'),
            'password' => $pw_hash,
            'name' => $this->input->post('name'),
            'birthdate' => $this->input->post('birthdate'),
            'personal_number' => $this->input->post('personal_number'),
            'type' => 1,
            'phone' => $this->input->post('phone'),
            'address' => $this->input->post('address'),
            'city_id' => $this->input->post('city'),
            'time_created' => date('Y-m-d H:i:s')
        );

        return $this->db->insert('users', $data);
    }

    public function update_users($id = FALSE, $photo_url = NULL) {
        if ($id == FALSE) {
            return FALSE;
        }

        $data = array(
            'name' => $this->input->post('name'),
            'birthdate' => $this->input->post('birthdate'),
            'personal_number' => $this->input->post('personal_number'),
            'phone' => $this->input->post('phone'),
            'photo' => $photo_url,
            'address' => $this->input->post('address'),
            'city_id' => $this->input->post('city')
        );

        $this->db->where('id', $id);
        return $this->db->update('users', $data);
    }

    public function activate_user($email) {
        if ($email == NULL) {
            return FALSE;
        }

        $data = array('type' => 2);
        $this->db->where('email', $email);
        return $this->db->update('users', $data);
    }

    public function verify_login() {
        $email = $this->input->post('email_login');
        $password = $this->input->post('password_login');
        $this->db->select('id, email, password, name, type');
        $query = $this->db->get_where('users', array('email' => $email));
        if ($query->num_rows() == 1) {
            $row = $query->row_array();
            if (hash_equals($row['password'], crypt($password, $row['password']))) {
                return array(
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'type' => $row['type']
                );
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
}

?>