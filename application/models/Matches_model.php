<?php
class Matches_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_matches($tournament_id = NULL, $round = NULL) {
        if ($tournament_id == NULL) {
            return FALSE;
        }

        if ($round != NULL) {
            $query = $this->db->select(
                'matches.*,
                p1.name AS partic1_name,
                p2.name AS partic2_name'
            )
            ->from('matches')
            ->join('participants AS p1', 'matches.partic1_id = p1.id', 'left')
            ->join('participants AS p2', 'matches.partic2_id = p2.id', 'left')
            ->where('matches.tournament_id', $tournament_id)
            ->where('matches.round', $round)
            ->get();
            return $query->result_array();
        }

        $arr = array();
        $data = array();
        $query = $this->db->select(
            'matches.*,
            p1.name AS partic1_name,
            p2.name AS partic2_name'
        )
        ->from('matches')
        ->join('participants AS p1', 'matches.partic1_id = p1.id', 'left')
        ->join('participants AS p2', 'matches.partic2_id = p2.id', 'left')
        ->where('matches.tournament_id', $tournament_id)
        ->get();
        $arr = $query->result_array();
        foreach ($arr as $a) {
            $round = $a['round'];
            $match = $a['id'];
            $data[$round][$match] = $a;
        }

        return $data;
    }

    public function set_swiss_matches($p1, $p2, $tournament_id, $round) {
        if ($tournament_id == NULL || $round == NULL) {
            return FALSE;
        }

        if (($p1 == NULL || $p2 == NULL))  {
            return FALSE;
        }

        $data = array(
            'tournament_id' => $tournament_id,
            'partic1_id' => $p1,
            'partic2_id' => $p2,
            'type' => 1, // Swiss
            'round' => $round,
            'time_created' => date('Y-m-d H:i:s')
        );
        $this->db->insert('matches', $data);
        return $last_id = $this->db->insert_id();
    }

    public function previous_opponents($partic_id) {
        $query = $this->db->select('partic1_id, partic2_id')
        ->from('matches')
        ->where('partic1_id', $partic_id)
        ->or_where('partic2_id', $partic_id)
        ->get();
        foreach ($query->result() as $match) {
            if ($match->partic1_id == $partic_id) {
                $data[] = $match->partic2_id;
            } else if ($match->partic2_id == $partic_id) {
                $data[] = $match->partic1_id;
            }
        }

        return array_unique($data);
    }

    public function set_elim_matches($p1, $p2, $tournament_id, $round) {
        if ($p1 == NULL || $p2 == NULL || $tournament_id == NULL || $round == NULL) {
            return FALSE;
        }

        $data = array(
            'tournament_id' => $tournament_id,
            'partic1_id' => $p1,
            'partic2_id' => $p2,
            'type' => 2, // Swiss
            'round' => $round,
            'time_created' => date('Y-m-d H:i:s')
        );
        $this->db->insert('matches', $data);
        return $this->db->insert_id();
    }

    public function submit_scores($id, $score1, $score2) {
        if ($id == NULL || $score1 == NULL || $score2 == NULL) {
            return 0;
        }
        
        $data = array(
            'partic1_score' => $score1,
            'partic2_score' => $score2
        );
        $this->db->where('id', $id);
        $this->db->update('matches', $data);
        return ($this->db->affected_rows() > 0) ? 1 : 0;
    }

    public function remove_bulk_matches($tournament_id) {
        if ($tournament_id == NULL) {
            return FALSE;
        }
        
        $this->db->where('tournament_id', $tournament_id);
        return $this->db->delete('matches');
    }
}
?>