<?php
class Articles_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_articles($slug = NULL) {
        // Return all articles, sorted by newest creation time
        if ($slug == NULL) {
            $query = $this->db->select(
                'articles.*,
                users.name AS writer'
            )
            ->from('articles')
            ->join('users', 'users.id = articles.user_id')
            ->where('published', 1)
            ->order_by('articles.time_created DESC')
            ->get();
            return $query->result_array();
        }
        
        // Otherwise, get a single article by slug
        $query = $this->db->select(
            'articles.*,
            users.name AS writer'
        )
        ->from('articles')
        ->join('users', 'users.id = articles.user_id')
        ->where('articles.slug', $slug)
        ->where('published', 1)
        ->get();
        return $query->row_array();
    }

    public function get_user_articles($user_id) {
        $query = $this->db->select('*')
        ->from('articles')
        ->where('user_id', $user_id)
        ->order_by('articles.time_created')
        ->get();
        return $query->result_array();
    }

    public function get_articles_by_id ($id = NULL) {
        if ($id == NULL) {
            return FALSE;
        }

        $query = $this->db->select('*')
        ->from('articles')
        ->where('articles.id', $id)
        ->get();
        return $query->row_array();
    }

    public function get_latest_article() {
        $query = $this->db->select(
            'articles.*,
            users.name AS writer'
        )
        ->from('articles')
        ->join('users', 'users.id = articles.user_id')
        ->where('published', 1)
        ->order_by('time_created DESC')
        ->limit(1)
        ->get();
        return $query->row_array();
    }

    public function set_articles($user = NULL) {
        if ($user == NULL) {
            return FALSE;
        }
        
        // Create clean URL title
        $this->load->helper('url');
        $slug = url_title($this->input->post('title'), 'dash', TRUE);
        $published = FALSE;
        // Set $published depending on submit method
        if (isset($_POST['save_draft'])) {$published = FALSE;}
        else if (isset($_POST['save_publish'])) {$published = TRUE;}
        else {return FALSE;}

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'user_id' => $user,
            'content' => $this->input->post('content'),
            'published' => $published,
            'time_created' => date('Y-m-d H:i:s')
        );

        return $this->db->insert('articles', $data);
    }

    public function update_articles($id = NULL) {
        if ($id == NULL) {
            return FALSE;
        }

        $this->load->helper('url');
        $slug = url_title($this->input->post('title'), 'dash', TRUE);
        $published = FALSE;
        if (isset($_POST['save_draft'])) {$published = FALSE;}
        else if (isset($_POST['save_publish'])) {$published = TRUE;}
        else {return FALSE;}

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'content' => $this->input->post('content'),
            'published' => $published
        );

        $this->db->where('id', $id);
        return $this->db->update('articles', $data);
    }

    public function delete_articles($id = NULL) {
        if ($id == NULL) {
            return FALSE;
        }

        $this->db->where('id', $id);
        return $this->db->delete('articles');
    }
}
?>