<?php
class Banners extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('banners_model');
    }

    public function create() {
        if ($this->session->userdata('logged_user') === NULL || $_SESSION['logged_user']['type'] != 0) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('url', 'URL', 'required|valid_url');
        $data['title'] = 'Create New Banner';
        $img_path = NULL;

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('banners/create');
            $this->load->view('templates/footer.php');
        } else {
            $insert_id = $this->banners_model->set_banners();

            $config['file_name'] = 'banner'.$insert_id;
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_width']  = '1000';
            $config['max_height']  = '300';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('img')) {
                $this->session->set_flashdata('msg', $this->upload->display_errors());
                redirect('/banners/manage/','refresh');
            } else {
                $ext = $this->upload->data('file_ext');
                $img_path = '/uploads/banner'.$insert_id.$ext;
            }

            $this->banners_model->update_banners($insert_id, $img_path);
            $this->session->set_flashdata('msg', 'Your banner has been submitted.');
            redirect('/banners/manage','refresh');
        }
    }

    public function edit($id = NULL) {
        if ($this->session->userdata('logged_user') === NULL || $_SESSION['logged_user']['type'] != 0) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }
        if ($id == NULL) {
            $this->session->set_flashdata('msg', 'No ID provided.');
            redirect('/','refresh');
        }
        $data['banners_item'] = $this->banners_model->get_banners($id);
        if (empty($data['banners_item'])) {
            $this->session->set_flashdata('msg', 'No item found with the provided ID');
            redirect('/','refresh');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('url', 'URL', 'required|valid_url');
        $data['title'] = 'Edit Banner';
        $img_path = NULL;

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('banners/edit', $data);
            $this->load->view('templates/footer.php');
        } else if (empty($_FILES['img']['name'])) {
            $this->banners_model->update_banners($id, $img_path);
            $this->session->set_flashdata('msg', 'Banner updated.');
            redirect('/','refresh');
        } else {
            $config['file_name'] = 'banner'.$insert_id;
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_width']  = '1000';
            $config['max_height']  = '300';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('img')) {
                $this->session->set_flashdata('msg', $this->upload->display_errors());
                redirect('/banners/manage/','refresh');
            } else {
                $ext = $this->upload->data('file_ext');
                $img_path = '/uploads/banner'.$id.$ext;
            }

            $this->banners_model->update_banners($insert_id, $img_path);
            $this->session->set_flashdata('msg', 'Your banner has been submitted.');
            redirect('/banners/manage','refresh');
        }
    }

    public function manage() {
        $data['banners'] = $this->banners_model->get_banners();
        $data['title'] = 'Manage Banners';

        $this->load->view('templates/header', $data);
        $this->load->view('banners/manage', $data);
        $this->load->view('templates/footer');
    }

    public function delete($id) {
        if ($this->session->userdata('logged_user') === NULL || $_SESSION['logged_user']['type'] != 0) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }
        if ($id == NULL) {
            $this->session->set_flashdata('msg', 'No ID provided.');
            redirect('/','refresh');
        }
        $data['banners_item'] = $this->banners_model->get_banners($id);
        if (empty($data['banners_item'])) {
            $this->session->set_flashdata('msg', 'No item found with the provided ID');
            redirect('/','refresh');
        }

        $this->banners_model->delete_banners($id);
        unlink('.'.$data['banners_item']['img_path']);
        $this->session->set_flashdata('msg', 'Your banner has been submitted.');
        redirect('/banners/manage','refresh');
    }
}
?>