<?php
class Matches extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('tournaments_model');
        $this->load->model('participants_model');
        $this->load->model('matches_model');
    }

    public function index($tournament_id) {
        // Check whether the parameter is set
        if ($tournament_id == NULL) {
            $this->session->set_flashdata('msg', 'No tournament selected.');
            redirect('/tournaments','refresh');
        }

        $data['tournament'] = $this->tournaments_model->get_tournaments($tournament_id);
        if (empty($data['tournament'])) {
            $this->session->set_flashdata('msg', 'Tournament not found');
            redirect('/tournaments','refresh');
        }

        $data['matches'] = $this->matches_model->get_matches($tournament_id);
        $data['title'] = 'Match List of '.$data['tournament']['name'];

        $this->load->view('templates/header.php', $data);
        $this->load->view('matches/index', $data);
        $this->load->view('templates/footer.php');
    }

    public function manage($tournament_id) {
        // Check whether there are any logged users
        if ($this->session->userdata('logged_user') === NULL) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments','refresh');
        }

        // Check whether the tournament_id is set in parameter
        if ($tournament_id === NULL) {
            $this->session->set_flashdata('msg', 'Please select a tournament first and then select "Manage Participants" button.');
            redirect('/tournaments', 'refresh');
        }

        // Get and check whether the tournament exist, then get matches if it exists
        $data['tournament'] = $this->tournaments_model->get_tournaments($tournament_id);
        if (empty($data['tournament'])) {
            $this->session->set_flashdata('msg', 'Tournament not found. Please select an available tournament from the list.');
            redirect('/tournaments', 'refresh');
        } else {
            // Check whether the logged user is the tournament manager
            if ($data['tournament']['manager_id'] != $_SESSION['logged_user']['id']) {
                $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
                redirect('/', 'refresh');
            }

            $data['matches'] = $this->matches_model->get_matches($tournament_id);
        }
        
        $data['title'] = 'Match Management of '.$data['tournament']['name'];

        $this->load->view('templates/header.php', $data);
        $this->load->view('matches/manage', $data);
        $this->load->view('templates/footer.php');
    }

    public function view($id) {

    }

    public function edit($id) {

    }

    public function printout($tournament_id, $round) {
        // Check whether the parameter is set
        if ($tournament_id == NULL || $round == NULL) {
            $this->session->set_flashdata('msg', 'No tournament selected.');
            redirect('/tournaments','refresh');
        }

        $data['tournament'] = $this->tournaments_model->get_tournaments($tournament_id);
        if (empty($data['tournament']) || $data['tournament']['current_rounds'] < $round) {
            $this->session->set_flashdata('msg', 'Tournament not valid');
            redirect('/tournaments','refresh');
        }

        $data['matches'] = $this->matches_model->get_matches($tournament_id, $round);
        $this->load->view('matches/printout', $data);
    }

    public function generate($tournament_id) {
        // Check whether there are any logged users
        if ($this->session->userdata('logged_user') == NULL) {
            $this->session->set_flashdata('msg', 'You do not have permission to do that action');
            redirect('/','refresh');
        }

        // Check whether the tournament with that ID exist
        $tournament = $this->tournaments_model->get_tournaments($tournament_id);
        if (empty($tournament)) {
            $this->session->set_flashdata('msg', 'No tournaments found with ID = '.$tournament_id);
            redirect('/','refresh');
        }

        // Check whether the logged user is the tournament manager
        if ($tournament['manager_id'] != $_SESSION['logged_user']['id']) {
            $this->session->set_flashdata('msg', 'You do not have permission to do that action');
            redirect('/','refresh');
        }

        // Check whether there are 2 or more approved participants in that tournament
        /*
        $participants = $this->participants_model->get_participants_ids($tournament_id);
        if (empty($participants) || count($participants) < 2) {
            $this->session->set_flashdata('msg', 'Not enough approved participants (min. 2)');
            redirect('/tournaments/'.$tournament_id,'refresh');
        }
        */
        
        // 1st Swiss round
        if ($tournament['type'] == 1 && $tournament['current_rounds'] == 0) {
            generate_swiss_matches($tournament);
            $this->session->set_flashdata('msg', 'Matches generated. Registration closed.');
            redirect('/matches/manage/'.$tournament_id, 'refresh');
        }
        // 2nd and further Swiss rounds
        else if ($tournament['type'] == 1 && $tournament['current_rounds'] > 0) {
            generate_swiss_matches($tournament);
            $this->session->set_flashdata('msg', 'Matches generated.');
            redirect('/matches/manage/'.$tournament_id, 'refresh');
        }
        // Elimination Match
        else if ($tournament['type'] == 2) {
            generate_elim_matches($tournament);
            $this->session->set_flashdata('msg', 'Matches generated.');
            redirect('/matches/manage/'.$tournament_id, 'refresh');
        }
    }

    public function submit() {
        $id = $this->input->post('id');
        $score1 = $this->input->post('score1');
        $score2 = $this->input->post('score2');
        echo $this->matches_model->submit_scores($id, $score1, $score2);
    }

}
?>