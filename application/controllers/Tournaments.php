<?php
class Tournaments extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cities_model');
        $this->load->model('tournaments_model');
        $this->load->model('participants_model');
        $this->load->model('matches_model');
    }

    public function index() {
        $data['tournaments'] = $this->tournaments_model->get_tournaments();
        $data['title'] = 'Available Tournaments';
        
        $this->load->view('templates/header.php', $data);
        $this->load->view('tournaments/index', $data);
        $this->load->view('templates/footer.php');
    }

    public function past() {
        $data['tournaments'] = $this->tournaments_model->get_past_tournaments();
        $data['title'] = 'Past Tournaments';
        
        $this->load->view('templates/header.php', $data);
        $this->load->view('tournaments/index', $data);
        $this->load->view('templates/footer.php');
    }

    public function create() {
        if ($this->session->userdata('logged_user') === NULL || ($_SESSION['logged_user']['type'] != 0 && $_SESSION['logged_user']['type'] != 2)) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments','refresh');
        }

        $this->load->library('form_validation');

        $data['cities'] = $this->cities_model->get_cities();
        $data['title'] = 'New Tournament';

        $this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[a-zA-Z0-9!-_\s]*$/]');
        // Start Time and End Time needs more validation rules
        $this->form_validation->set_rules('start_time', 'Start Time', 'required');
        $this->form_validation->set_rules('end_time', 'End Time', 'required');
        $this->form_validation->set_rules('max_partic', 'Max Participants', 'required|numeric');
        $this->form_validation->set_rules('max_rounds', 'Max Rounds', 'numeric');
        $this->form_validation->set_rules('address', 'Address', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('tournaments/create', $data);
            $this->load->view('templates/footer.php');
        } else {
            $user = $_SESSION['logged_user']['id'];
            $this->tournaments_model->set_tournaments($user);
            $this->session->set_flashdata('msg', 'Your tournament has been created!');
            redirect('/tournaments','refresh');
        }
    }

    public function view($id = NULL) {
        $data['tournaments_item'] = $this->tournaments_model->get_tournaments($id);

        if(empty($data['tournaments_item'])) {
            show_404();
        }

        $data['participants'] = $this->participants_model->get_approved_participants($id);
        $data['title'] = 'Tournament Details';
        $this->load->view('templates/header', $data);
        $this->load->view('tournaments/view', $data);
        $this->load->view('templates/footer');
    }

    public function manage($user_id = NULL) {
        if ($this->session->userdata('logged_user') === NULL) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }

        if ($user_id == NULL) {
            $this->session->set_flashdata('msg', 'No ID provided');
            redirect('/','refresh');
        }

        if ($user_id != $_SESSION['logged_user']['id']) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }

        $data['tournaments'] = $this->tournaments_model->get_user_tournaments($user_id);
        $data['title'] = 'Your Tournaments';
        
        $this->load->view('templates/header', $data);
        $this->load->view('tournaments/manage', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = NULL) {
        if ($this->session->userdata('logged_user') === NULL) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments','refresh');
        }

        $data['tournaments_item'] = $this->tournaments_model->get_tournaments($id);

        if (empty($data['tournaments_item'])) {
            show_404();
        }
        if ($data['tournaments_item']['manager_id'] != $_SESSION['logged_user']['id']) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/', 'refresh');
        }

        $this->load->library('form_validation');

        $data['cities'] = $this->cities_model->get_cities();
        $data['title'] = 'Edit Tournament';
        $logo_url = NULL;

        $this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[a-zA-Z0-9!-_\s]*$/]');
        // Start Time and End Time needs more validation rules
        $this->form_validation->set_rules('start_time', 'Start Time', 'required');
        $this->form_validation->set_rules('end_time', 'End Time', 'required');
        $this->form_validation->set_rules('max_partic', 'Max Participants', 'required|numeric');
        $this->form_validation->set_rules('max_rounds', 'Max Rounds', 'numeric');
        $this->form_validation->set_rules('address', 'Address', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('tournaments/edit', $data);
            $this->load->view('templates/footer.php');
        } else if (empty($_FILES['logo']['name'])) {
            $this->tournaments_model->update_tournaments($id, $logo_url);
            $this->session->set_flashdata('msg', 'Your tournament has been updated!');
            redirect('/tournaments/'.$id,'refresh');
        } else {
            // Do upload after form validation
            $config['file_name'] = 'tournament'.$id;
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']  = '150';
            $config['max_width']  = '1024';
            $config['max_height']  = '1024';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('logo')) {
                $this->session->set_flashdata('msg', $this->upload->display_errors());
                redirect('/tournaments/view/'.$id,'refresh');
            } else {
                $ext = $this->upload->data('file_ext');
                $logo_url = '/uploads/tournament'.$id.$ext;
            }

            $this->tournaments_model->update_tournaments($id, $logo_url);
            $this->session->set_flashdata('msg', 'Your tournament has been updated!');
            redirect('/tournaments/'.$id,'refresh');
        }
    }

    public function live($id = NULL){
        $data['tournaments_item'] = $this->tournaments_model->get_tournaments($id);

        if(empty($data['tournaments_item'])) {
            $this->session->set_flashdata('msg', 'No tournament selected.');
            redirect('/tournaments/','refresh');
        }

        $data['title'] = 'Featured Live Match';
        $this->load->view('templates/header', $data);
        $this->load->view('tournaments/live', $data);
        $this->load->view('templates/footer');
    }

    public function reset($id = NULL) {
        if ($this->session->userdata('logged_user') === NULL) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments/','refresh');
        }
        $data['tournaments_item'] = $this->tournaments_model->get_tournaments($id);
        if (empty($data['tournaments_item'])) {
            show_404();
        }
        if ($data['tournaments_item']['manager_id'] != $_SESSION['logged_user']['id']) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/', 'refresh');
        }

        $this->tournaments_model->reset_tournament($id);
        $this->matches_model->remove_bulk_matches($id);
        $this->participants_model->reset_standing($id);
        $this->participants_model->remove_dummy($id);

        $this->session->set_flashdata('msg', 'Your tournament has been reset. All matches have been permanently deleted.');
        redirect('/matches/manage/'.$id,'refresh');
    }
}
?>