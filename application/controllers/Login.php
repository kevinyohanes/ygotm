<?php
class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('users_model');                
    }

    public function index() {
        if (NULL !== $this->session->userdata('logged_user')) {
            redirect($this->session->userdata('/'),'refresh');
        }

        $data['title'] = 'Login';

        $this->form_validation->set_rules('email_login', 'email_login', 'trim|required|valid_email');
        $this->form_validation->set_rules('password_login', 'password_login', 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('login/index');
            $this->load->view('templates/footer');
        } else {
            $user_data = $this->users_model->verify_login();
            if ($user_data != FALSE) {
                $this->session->set_userdata('logged_user', $user_data);
                if ($user_data['type'] == 1) {
                    $this->session->set_flashdata('msg', 'Reminder: You have not activated your e-mail account. Please check your e-mail');
                }
                redirect($this->session->userdata('referer'),'refresh');
            } else {
                $data['login_error'] = 'Login failed. Please try again or contact the administrator for help.';
                $this->load->view('templates/header', $data);
                $this->load->view('login/index', $data);
                $this->load->view('templates/footer');
            }
        }
    }

    public function logout() {
        $this->session->unset_userdata('logged_user');
        $this->session->sess_destroy();
        redirect('/', 'refresh');
    }
}
?>