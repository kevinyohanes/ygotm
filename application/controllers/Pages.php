<?php
class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('articles_model');
        $this->load->model('banners_model');
        $this->load->model('tournaments_model');
    }
    
    public function view($page = 'home') {
        // if there is no file matched the URL, show 404 error
        if (!file_exists(APPPATH.'views/pages/'.$page.'.php')) {
            show_404();
        }

        // Set the title by the page name
        if ($page == 'faq') {
            $data['title'] = 'Frequently Asked Question';
        } else {
            $data['title'] = ucfirst($page);
        }
        $data['banners'] = $this->banners_model->get_banners();
        $data['latest_article'] = $this->articles_model->get_latest_article();
        $data['articles'] = $this->articles_model->get_articles();
        $data['tournaments'] = $this->tournaments_model->get_tournaments();

        // Load the view in order
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
}
?>