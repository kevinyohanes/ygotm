<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {
    public function image_upload() {
        if (defined('BASEPATH') && !$this->input->is_ajax_request()) 
        exit('No direct script access allowed');

        $dir_name = "uploads/";
        $timestamp = date('dMY_His');
        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        move_uploaded_file($_FILES['file']['tmp_name'], $dir_name.'img_'.$timestamp.'.'.$ext);
        echo 'img_'.$timestamp.'.'.$ext;
    }

    public function image_delete() {
        if (defined('BASEPATH') && !$this->input->is_ajax_request()) 
        exit('No direct script access allowed');
        $filename = $this->input->post('filename');
        if ($filename == NULL) {
            echo 'no path';
        }
        $path = './uploads/'.$filename;
        if (!is_file($path)) {
            echo 'file not found';
        }
        if (unlink($path)) {
            echo 'Delete Success';
        } else {
            echo 'Delete Failed';
        }
    }
}
?>