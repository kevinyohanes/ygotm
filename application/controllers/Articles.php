<?php
class Articles extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('articles_model');
    }

    public function index() {
        $data['articles'] = $this->articles_model->get_articles();
        $data['title'] = 'Articles Archive';

        $this->load->view('templates/header.php', $data);
        $this->load->view('articles/index', $data);
        $this->load->view('templates/footer.php');
    }

    public function create() {
        if ($this->session->userdata('logged_user') === NULL || ($_SESSION['logged_user']['type'] != 0 && $_SESSION['logged_user']['type'] != 3)) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data['title'] = 'New Article';
        
        $this->form_validation->set_rules('title', 'Title', 'required|is_unique[articles.title]');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('articles/create', $data);
            $this->load->view('templates/footer.php');
        } else {
            $user = $_SESSION['logged_user']['id'];
            $this->articles_model->set_articles($user);
            $this->session->set_flashdata('msg', 'Your article has been submitted!');
            redirect('/','refresh');
        }
    }

    public function view($slug) {
        $data['articles_item'] = $this->articles_model->get_articles($slug);

        if (empty($data['articles_item'])) {
            $this->session->set_flashdata('msg', 'No articles found with that URL');
            redirect('/articles','refresh');
        }

        $data['title'] = $data['articles_item']['title'];
        $this->load->view('templates/header.php', $data);
        $this->load->view('articles/view', $data);
        $this->load->view('templates/footer.php');
    }

    public function edit($id = NULL) {
        if ($this->session->userdata('logged_user') === NULL || ($_SESSION['logged_user']['type'] != 0 && $_SESSION['logged_user']['type'] != 3)) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }
        
        $data['articles_item'] = $this->articles_model->get_articles_by_id($id);

        if ($_SESSION['logged_user']['id'] != $data['articles_item']['user_id']) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['title'] = 'Edit Article';
        
        // TODO: Needs UNIQUE validation outside MySQL, is_unique() fails if the title is not changed
        $this->form_validation->set_rules('title', 'Title', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('articles/edit', $data);
            $this->load->view('templates/footer.php');
        } else {
            $this->articles_model->update_articles($id);
            $this->session->set_flashdata('msg', 'Your article has been submitted!');
            redirect('/','refresh');
        }
    }

    public function delete($id = NULL) {
        $data['articles_item'] = $this->articles_model->get_articles_by_id($id);
        
        if (empty($data['articles_item'])) {
            $this->session->set_flashdata('msg', 'No articles found with that ID');
            redirect('/articles','refresh');
        }

        if ($this->session->userdata('logged_user') === NULL || $data['articles_item']['user_id'] != $_SESSION['logged_user']['id']) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }

        if ($this->articles_model->delete_articles($id)) {
            $this->session->set_flashdata('msg', 'Articles Deleted');
            redirect('/articles/manage/'.$_SESSION['logged_user']['id'],'refresh');
        }
    }

    public function manage($user_id = NULL) {
        if ($this->session->userdata('logged_user') === NULL || ($_SESSION['logged_user']['type'] != 0 && $_SESSION['logged_user']['type'] != 3)) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }

        if ($user_id == NULL) {
            $this->session->set_flashdata('msg', 'No ID provided');
            redirect('/','refresh');
        }

        if ($user_id != $_SESSION['logged_user']['id']) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/','refresh');
        }

        $data['articles'] = $this->articles_model->get_user_articles($user_id);
        $data['title'] = 'Your Articles';
        
        $this->load->view('templates/header', $data);
        $this->load->view('articles/manage', $data);
        $this->load->view('templates/footer');
    }
}
?>