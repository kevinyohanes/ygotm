<?php
class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('cities_model');
        $this->load->helper('hasher');
    }

    public function index() {
        $data['users'] = $this->users_model->get_users();
        $data['title'] = 'Duelist Index';

        $this->load->view('templates/header.php', $data);
        $this->load->view('users/index', $data);
        $this->load->view('templates/footer.php');
    }

    public function create() {
        // For alpha test, create() will be disabled
        //$this->session->set_flashdata('msg', 'This function is disabled temporarily in pre-alpha stage. We are sorry for the inconvenience.');
        //redirect('/','refresh');
        
        $this->load->library('form_validation');        

        $data['cities'] = $this->cities_model->get_cities();
        $data['title'] = 'User Registration';

        $this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[a-zA-Z\s]*$/]');
        // Birthdate still needs more validation rules
        $this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_rules('personal_number', 'Personal Number', 'numeric');
        $this->form_validation->set_rules('phone', 'Phone', 'required|numeric');
        $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'callback_recaptcha');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('users/create', $data);
            $this->load->view('templates/footer.php');
        } else {
            $this->users_model->set_users();
            $key = base64_url_encode($_POST['email']);
            $this->send_verification($_POST['email'], $key);
            $this->session->set_flashdata('msg', 'Your account has been created! Please verify your identity from the link we have send to your e-mail address.');
            redirect('/','refresh');
        }
    }

    public function resend_verification($id) {
        if ($id == NULL) {
            $this->session->set_flashdata('msg', 'Invalid E-mail');
            redirect('/','refresh');
        }
        $account = $this->users_model->get_users($id);
        $key = base64_url_encode($account['email']);
        $this->send_verification($account['email'], $key);
        $this->session->set_flashdata('msg', 'Please verify your identity from the link we have send to your e-mail address.');
        redirect('/','refresh');
    }

    function recaptcha($str = NULL) {
        $fields = array(
            'secret'    =>  "6Lc-IzMUAAAAAB1Z-66fNbineTSUDqhTfaxtz4HM",
            'response'  =>  $str,
            'remoteip'  =>  $_SERVER['REMOTE_ADDR']
        );
        $ch = curl_init("https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        $res = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($res, true);
        if($res['success']) {
            return TRUE;
        } else {
            $this->form_validation->set_message('recaptcha', 'The reCAPTCHA field is telling me that you are a robot. Shall we give it another try?');
            return FALSE;
        }
    }

    function send_verification($email, $key) {
        if ($email == NULL || $key == NULL) {
            return FALSE;
        }

        // Send confirmation email
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->from('no-reply@yic-today.com', 'YIC Today');
        $this->email->to($email);
        $this->email->subject('YIC Today Account Verification');
        $url = base_url()."users/verify/".$key;

        $email_msg = "<html><head><head></head><body><p>Hi,</p><p>Thanks for registration with YIC Today.</p><p>Please click below link to verify your email.</p>".$url."<br/><p>Sincerely,</p><p>YIC Today Team</p></body></html>";
        $this->email->message($email_msg);

        return $this->email->send();
    }

    public function verify($key) {
        if ($key == NULL) {
            $this->session->set_flashdata('msg', 'No key provided');
            redirect('/','refresh');
        }
        $this->load->helper('hasher');
        $email = base64_url_decode($key);
        if ($this->users_model->activate_user($email)) {
            $this->session->set_flashdata('msg', 'Account activated! Log in and start a new tournament now!');
            redirect('/','refresh');
        } else {
            $this->session->set_flashdata('msg', 'Your activation key is not valid');
            redirect('/','refresh');
        }
    }

    public function view($id = NULL) {
        $data['users_item'] = $this->users_model->get_users($id);

        if (empty($data['users_item'])) {
            show_404();
        }

        $data['title'] = $data['users_item']['name'].' Profile';
        $this->load->view('templates/header', $data);
        $this->load->view('users/view', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = NULL) {
        if ($this->session->userdata('logged_user') === NULL || $_SESSION['logged_user']['id'] != $id) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/', 'refresh');
        }

        $data['users_item'] = $this->users_model->get_users($id);
        $photo_url = NULL;

        if (empty($data['users_item'])) {
            show_404();
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['cities'] = $this->cities_model->get_cities();
        $data['title'] = 'Edit Profile';

        $this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[a-zA-Z\s]*$/]');
        // Birthdate still needs more validation rules
        $this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
        $this->form_validation->set_rules('personal_number', 'Personal Number', 'numeric');
        $this->form_validation->set_rules('phone', 'Phone', 'required|numeric');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('users/edit', $data);
            $this->load->view('templates/footer.php');
        } else if (empty($_FILES['photo']['name'])) {
            $this->users_model->update_users($id, $photo_url);
            $this->session->set_flashdata('msg', 'Your profile has been updated. No photo uploaded.');
            redirect('/users/'.$id,'refresh');
        } else {
            // Do upload after form validation
            $config['file_name'] = 'user'.$id;
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']  = '150';
            $config['max_width']  = '1024';
            $config['max_height']  = '1024';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('photo')) {
                $this->session->set_flashdata('msg', $this->upload->display_errors());
                redirect('/users/'.$id,'refresh');
            } else {
                $ext = $this->upload->data('file_ext');
                $photo_url = '/uploads/user'.$id.$ext;
            }

            $this->users_model->update_users($id, $photo_url);
            $this->session->set_flashdata('msg', 'Your profile has been updated.');
            redirect('/users/'.$id,'refresh');
        }
    }
}

?>