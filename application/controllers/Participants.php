<?php
class Participants extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('participants_model');
        $this->load->model('tournaments_model');
        $this->load->model('cities_model');
    }

    function index ($tournament_id = NULL) {
        // Check whether there are any logged users
        if ($this->session->userdata('logged_user') === NULL) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments','refresh');
        }

        // Check whether the tournament_id is set in parameter
        if ($tournament_id === NULL) {
            $this->session->set_flashdata('msg', 'Please select a tournament first and then select "Manage Participants" button.');
            redirect('/tournaments', 'refresh');
        }

        // Get and check whether the tournament exist, then get participants list if it exists
        $data['tournament'] = $this->tournaments_model->get_tournaments($tournament_id);
        if (empty($data['tournament'])) {
            $this->session->set_flashdata('msg', 'Tournament not found. Please select an available tournament from the list.');
            redirect('/tournaments', 'refresh');
        } else {
            // Check whether the logged user is the tournament manager
            if ($data['tournament']['manager_id'] != $_SESSION['logged_user']['id']) {
                $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
                redirect('/', 'refresh');
            }

            $data['participants'] = $this->participants_model->get_participants(FALSE, $tournament_id);
        }

        return $data;
    }

    public function index_ajax($tournament_id = NULL) {
        $data = $this->index($tournament_id);
        $this->load->view('participants/manage', $data);
    }

    public function create($tournament_id = NULL) {
        if ($tournament_id === NULL) {
            $this->session->set_flashdata('msg', 'Please select a tournament first and then select "Participate" button.');
            redirect('/tournaments', 'refresh');
        }

        $tournament = $this->tournaments_model->get_tournaments($tournament_id);

        if (empty($tournament)) {
            $this->session->set_flashdata('msg', 'Tournament not found. Please select an available tournament from the list.');
            redirect('/tournaments', 'refresh');
        }

        if (!$tournament['open']) {
            $this->session->set_flashdata('msg', 'Registration for this tournament has been closed.');
            redirect('/tournaments/'.$tournament_id,'refresh');
        }

        $this->load->library('form_validation');

        $data['cities'] = $this->cities_model->get_cities();
        $data['tournament_id'] = $tournament_id;
        $data['title'] = 'Duelist Registration';

        $this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[a-zA-Z\s]*$/]');
        // Birthdate still needs more validation rules
        $this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('personal_number', 'Personal Number', 'numeric');
        $this->form_validation->set_rules('phone', 'Phone', 'required|numeric');

        if (isset($_POST['email'])) {
            $partics = $this->participants_model->get_participants(FALSE, $tournament_id);
            foreach ($partics as $p) {
                if ($p['email'] == $_POST['email']) {
                    $this->session->set_flashdata('msg', 'The email '.$_POST['email'].' has been used to participate in this tournament.');
                    redirect($this->session->userdata('referer'), 'refresh');
                } 
            }
        }

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('participants/create', $data);
            $this->load->view('templates/footer.php');
        } else {
            $this->participants_model->set_participants($tournament_id);

            // Send confirmation email
            $this->load->library('email');
            $this->email->set_newline("\r\n");
            $this->email->from('no-reply@yic-today.com', 'YIC Today');
            $this->email->to($_POST['email']);

            $this->email->subject('YIC Today '.$tournament['name'].' Participation Confirmation');
            $emaildata['tournament'] = $tournament;
            $emaildata['partic'] = $_POST;
            // Get single city
            $emaildata['partic']['city'] = $this->cities_model->get_cities($_POST['city']);

            $email_msg = $this->load->view('templates/email_template.php', $emaildata, TRUE);
            $this->email->message($email_msg);

            $result = $this->email->send();

            $this->session->set_flashdata('msg', 'You have applied into the tournament! However, you need to be approved first by the Tournament Manager. Please check your email inbox (or spam) for further instructions.');
            redirect('/tournaments/view/'.$tournament_id, 'refresh');
        }
    }

    public function quick_create($tournament_id) {
        if (defined('BASEPATH') && !$this->input->is_ajax_request()) 
        exit('No direct script access allowed');
        if ($tournament_id === NULL) {
            return FALSE;
        }
        $tournament = $this->tournaments_model->get_tournaments($tournament_id);
        if (empty($tournament)) {
            return FALSE;
        }
        if ($tournament['manager_id'] != $_SESSION['logged_user']['id']) {
            return FALSE;
        }
        if (!$tournament['open']) {
            return FALSE;
        }
        if (isset($_POST['email'])) {
            $partics = $this->participants_model->get_participants(FALSE, $tournament_id);
            foreach ($partics as $p) {
                if ($p['email'] == $_POST['email']) {
                    return 'The email '.$_POST['email'].' has been used to participate in this tournament.';
                } 
            }
        }
        $this->participants_model->quick_set_participants($tournament_id);
    }

    public function view($id = NULL) {
        // Check whether the parameter is set
        if ($id == NULL) {
            $this->session->set_flashdata('msg', 'No ID is provided. Please select the participants from the list');
            redirect('/','refresh');
        }
        
        // Find and check whether the participant with that 'id' exists
        $data['participant'] = $this->participants_model->get_participants($id, NULL);
        if (empty($data['participant'])) {
            $this->session->set_flashdata('msg', 'No participants are found with ID = '.$id);
            redirect('/','refresh');
        }

        $data['title'] = $data['participant']['name'].' Profile';
        $this->load->view('templates/header', $data);
        $this->load->view('participants/view', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = NULL) {
        // Check whether there are any logged users
        if ($this->session->userdata('logged_user') === NULL) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments', 'refresh');
        }

        // Find and check whether the participant with that 'id' exists
        $data['participant'] = $this->participants_model->get_participants($id, NULL);
        if (empty($data['participant'])) {
            $this->session->set_flashdata('msg', 'No participants are found with ID = '.$id);
            redirect('/tournaments','refresh');
        }

        // Check whether the logged user is the tournament manager
        $tournament = $this->tournaments_model->get_tournaments($data['participant']['tournament_id']);
        if ($_SESSION['logged_user']['id'] != $tournament['manager_id']) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments', 'refresh');
        }
        
        // Set up the edit page and validation
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['cities'] = $this->cities_model->get_cities();
        $data['title'] = 'Edit Profile';

        $this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[a-zA-Z\s]*$/]');
        // Birthdate still needs more validation rules
        $this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
        $this->form_validation->set_rules('personal_number', 'Personal Number', 'numeric');
        $this->form_validation->set_rules('phone', 'Phone', 'required|numeric');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header.php', $data);
            $this->load->view('participants/edit', $data);
            $this->load->view('templates/footer.php');
        } else if (empty($_FILES['photo']['name'])) {
            $this->participants_model->update_participants($id, NULL);
            $this->session->set_flashdata('msg', 'The participant has been updated. No photo uploaded.');
            redirect('/participants/'.$id,'refresh');
        } else {
            // Do upload after form validation
            $config['file_name'] = 'participant'.$id;
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']  = '150';
            $config['max_width']  = '1024';
            $config['max_height']  = '1024';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('photo')) {
                $this->session->set_flashdata('msg', $this->upload->display_errors());
                redirect('/participants/'.$id,'refresh');
            } else {
                $ext = $this->upload->data('file_ext');
                $photo_url = '/uploads/participant'.$id.$ext;
            }

            $this->participants_model->update_participants($id, $photo_url);
            $this->session->set_flashdata('msg', 'Your profile has been updated.');
            redirect('/participants/'.$id,'refresh');
        }
    }

    public function manage($tournament_id = NULL) {
        $data = $this->index($tournament_id);

        // View the 'manage' page with participants list
        $data['title'] = 'Participants of '.$data['tournament']['name'];
        $this->load->view('templates/header', $data);
        $this->load->view('participants/manage', $data);
        $this->load->view('templates/footer');
    }

    public function set_type($id) {
        if (empty($id)) {
            echo FALSE;
        } else {
            echo $this->participants_model->set_partic_type($id);
        }
    }

    public function set_advantage ($id) {
        if (empty($id)) {
            echo FALSE;
        } else {
            echo $this->participants_model->set_partic_advantage($id);
        }
    }

    public function update_standings($tournament_id = NULL) {
        // Check whether there are any logged users
        if ($this->session->userdata('logged_user') === NULL) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments','refresh');
        }

        // Check whether the tournament_id is set in parameter
        if ($tournament_id === NULL) {
            $this->session->set_flashdata('msg', 'Please update the standings through "Manage" menu');
            redirect('/tournaments', 'refresh');
        }

        // Get and check whether the tournament exist
        $data['tournament'] = $this->tournaments_model->get_tournaments($tournament_id);
        if (empty($data['tournament'])) {
            $this->session->set_flashdata('msg', 'Tournament not found. Please select an available tournament from the list.');
            redirect('/tournaments', 'refresh');
        }

        // Check whether the logged user is the tournament manager
        if ($_SESSION['logged_user']['id'] != $data['tournament']['manager_id']) {
            $this->session->set_flashdata('msg', 'You do not have the permission to do that action.');
            redirect('/tournaments', 'refresh');
        }

        $partics = $this->participants_model->get_approved_participants($tournament_id);
        if ($data['tournament']['type'] == 1) {
            foreach ($partics as $p) {
                $this->participants_model->set_points($p['id']);
                $this->participants_model->set_mwp($p['id']);
                $this->participants_model->set_omw($p['id']);
                $this->participants_model->set_pgw($p['id']);
                $this->participants_model->set_ogw($p['id']);
            }
        } else if ($data['tournament']['type'] == 2) {
            foreach ($partics as $p) {
                $this->participants_model->set_points_elim($p['id']);
            }
        }
        $this->session->set_flashdata('msg', 'Standings updated. If you encounter any problems, please contact the administrator.');
        redirect('/tournaments/'.$tournament_id, 'refresh');
    }
}
?>