function sendFile (file, editor, welEditable) {
    var target = window.location.origin+"/image_upload/";
    data = new FormData();
    data.append("file", file);
    $.ajax({
        url: target,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function (data) {
            $("#content").summernote("insertImage", window.location.origin+'/uploads/'+data, data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus+" "+errorThrown);
        }
    });
}

function deleteFile (filename) {
    var url = window.location.origin+"/image_delete/";
    $.ajax({
        url: url,
        data: {'filename': filename},
        type: 'POST',
        success: function (data) {
            if (data) {
                alert('Success');
            } else {
                alert('Failed');
            }
        }
    })
}